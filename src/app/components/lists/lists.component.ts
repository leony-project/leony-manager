import { TypeofExpr } from '@angular/compiler';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConsumablesService } from 'src/app/consumables/consumables.service';
import { EquipmentsService } from 'src/app/equipments/equipments.service';
import { Consumable } from 'src/app/models/consumable';
import { Lists } from 'src/app/models/lists';
import { Tag } from 'src/app/models/tag';
import { AuthService } from 'src/app/security/auth.service';
import { TagsServices } from 'src/app/tags/tags-services.service';
import { environment } from 'src/environments/environment';
import { AlertService } from '../alert/alerts.service';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { TranslatorService } from 'src/app/translator/translator.service';


@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {
  faSearch = faSearch;
  tags: Tag[];
  actual: number = 1;
  @Input() lists: Lists;
  @Input() type: string;
  items: any;
  carouselItems: any
  restBaseUri = environment.base_endpoint;
  public cart: any[];
  constructor(
    public translator: TranslatorService,
    public auth: AuthService,
    public tagService: TagsServices,
    public consumableService: ConsumablesService,
    private alertService: AlertService,
    public equipmentService: EquipmentsService
  ) { }

  ngOnInit(): void {
    // this.loadAllTags();
    this.loadItems();
  }

  loadAllTags(): void {
    this.tagService.getAllTags().subscribe(data => {
      this.tags = data;
    });
  }

  loadItems(): void {
    if (this.type == 'CONSUMABLE') {
      this.consumableService.getAllConsumables().subscribe(data => {
        this.items = data;
        this.items.map(e => {
          e.url = "/consumables/" + e.id;
        });
        this.carouselItems = this.items;
      });
    } else if(this.type == 'EQUIPMENT') {
      this.equipmentService.getAll().subscribe(data => {
        this.items = data;
        this.items.map(e => {
          e.url = "/equipments/" + e.id;
          e.quantity = e.entitiesAvailable;
        });
        this.carouselItems = this.items;
        this.carouselItems.slice()
      });
    }
  }

  slide(type): void {
    if (type == 'NEXT') {
      if (this.items.length >= this.actual + 1) {
        this.actual++;
        this.showItem();
      }
    } else if (type == 'PREVIOUS') {
      if (this.actual - 1 >= 0) {
        this.actual--;
        this.showItem();
      }
    } else {
      console.log('MID')
    }
  }

  showItem(): void {
    this.items[this.actual].
    console.log(this.items[this.actual]);
  }

  deleteEntity(id) {
    let msg = this.translator.getSentence('ask-delete-these') + this.translator.getSentence(this.type.toLocaleLowerCase() + '-lower') + 's ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      if (this.type == 'EQUIPMENT') {
        this.equipmentService.toggleEquipmentEnabled(id).subscribe(
          {
          next: () => {
            this.loadItems();
            this.alertService.success(this.translator.getSentence('equip-deleted'), {
              keepAfterRouteChange: true,
            });
              
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });
      }  else if (this.type == 'CONSUMABLE') {
        this.consumableService.toggleConsumableEnabled(id).subscribe(
          {
          next: () => {
            this.alertService.success(this.translator.getSentence('consum-deleted'), {
              keepAfterRouteChange: true,
            });
              this.loadItems();
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });
      }
    }
  }

  addToCart(id): void {
    let that = this;
    that.cart = (JSON.parse(sessionStorage.getItem('cart'))== null ? [] : JSON.parse(sessionStorage.getItem('cart')));
    var result = that.cart.filter(function(e) {
      return [id].includes(e.id) && e.type == that.type
    });
    if (result.length == 0) {
      let item_to_cart = {id: id, quantity: 1, type: that.type};
      that.cart.push(item_to_cart);
      let udpate_cart = JSON.stringify(that.cart);
      sessionStorage.setItem('cart', udpate_cart);
    }
  }
  

}
