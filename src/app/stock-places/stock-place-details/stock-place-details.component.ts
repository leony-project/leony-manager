import { Component, OnInit } from '@angular/core';
import { StockPlace } from '../../models/stockPlace';
import { ActivatedRoute, Router } from '@angular/router';
import { StockPlaceService } from '../stock-place.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Equipment } from 'src/app/models/equipment';
import { Consumable } from 'src/app/models/consumable';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-stock-place-details',
  templateUrl: './stock-place-details.component.html',
  styleUrls: ['./stock-place-details.component.scss']
})
export class StockPlaceDetailsComponent implements OnInit {

  restBaseUri = environment.base_endpoint;
  stockPlace: Observable<StockPlace>;
  stockPlaceId: number
  public equipmentsPage = 1;
  public equipmentsPages: Array<number> = new Array<0>();
  public consumablesPage = 1;
  public consumablesPages: Array<number> = new Array<0>();
  private itemsNumberPerPage = 10;
  equipments: Equipment[];
  consumables: Consumable[];

  constructor(
    public translator: TranslatorService,
    private stockPlaceService: StockPlaceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {

    this.stockPlaceId = +this.activatedRoute.snapshot.paramMap.get('id');

    this.loadStockPlace(this.stockPlaceId);
    this.getEquipementList(this.stockPlaceId);
    this.getConsumableList(this.stockPlaceId);

  }

  loadStockPlace(id) {
    this.stockPlace = this.stockPlaceService.getOne(id)
  }

  getEquipementList(id) {
    this.stockPlaceService.getAllEquipementsStockPlace(id, this.stockPlaceId).subscribe((equipments: Equipment[]) => {
      this.equipmentsPages = Array.from(Array(Math.ceil(equipments['totalItems'] / this.itemsNumberPerPage)).keys());
      this.equipments = equipments;
    })
  }

  getConsumableList(id) {
    this.stockPlaceService.getAllConsumablesStockPlace(id, this.stockPlaceId).subscribe((consumables: Consumable[]) => {
      this.consumablesPages = Array.from(Array(Math.ceil(consumables['totalItems'] / this.itemsNumberPerPage)).keys());
      this.consumables = consumables;
    })
  }

  updatePage(n, keyword) {
    switch (keyword) {
      case 'equipments':
        this.equipmentsPage = n;
        this.getEquipementList(this.stockPlaceId);
        break;
      case 'consumables':
        this.consumablesPage = n;
        this.getConsumableList(this.stockPlaceId);
        break;
    }
  }

}
