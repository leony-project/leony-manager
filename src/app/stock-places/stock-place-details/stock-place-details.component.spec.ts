import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockPlaceDetailsComponent } from './stock-place-details.component';

describe('StockPlaceDetailsComponent', () => {
  let component: StockPlaceDetailsComponent;
  let fixture: ComponentFixture<StockPlaceDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockPlaceDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockPlaceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
