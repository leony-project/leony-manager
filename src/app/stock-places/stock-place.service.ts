import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StockPlace} from '../models/stockPlace';
import {Consumable} from "../models/consumable";
import { Observable } from 'rxjs';
import { Equipment } from '../models/equipment';

@Injectable({
  providedIn: 'root'
})
export class StockPlaceService {

  restBaseUri = environment.rest_endpoint;

  patchHeaders = new HttpHeaders({
    'Content-Type': 'application/merge-patch+json',
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    Accept: 'application/json'
  });

  fileHeaders = new HttpHeaders({
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    Accept: 'application/json'
  });

  constructor(private http: HttpClient) { }

  getAll(page = 1): Observable<StockPlace[]> {
    const url = this.restBaseUri + 'stock_places?page=' + page;
    return this.http.get<StockPlace[]>(url);
  }

  getOne(id): Observable<StockPlace> {
    const url = this.restBaseUri + 'stock_places/' + id;
    return this.http.get<StockPlace>(url);
  }
  toggleStockPlaceEnabled(id): Observable<StockPlace> {
    return this.http.post<StockPlace>(`${this.restBaseUri}stock_places/${id}/toggle_enabled`, {});
  }
  deleteItem(stockPlaceId): any {
    const url = this.restBaseUri + `stock_places/${stockPlaceId}`;
    return this.http.delete(url);
  }

  createStockPlace(params): any {
    const url = this.restBaseUri + 'stock_places';
    params.enabled = true;
    return this.http.post<StockPlace>(url, params);
  }

  patchStockPlace(id, changePayload): any {
    const url = this.restBaseUri + `stock_places/${id}`;
    return this.http.patch<StockPlace>(url, changePayload, {headers: this.patchHeaders});
  }

  getAllEquipementsStockPlace(stockPlaceId, page = 1): Observable<Equipment[]> {
    const url =`${this.restBaseUri}stock_places/${stockPlaceId}/equipments?page=`;
    return this.http.get<Equipment[]>(url + page );
  }
  getAllConsumablesStockPlace(stockPLaceId, page = 1): Observable<Consumable[]> {
    const url =`${this.restBaseUri}stock_places/${stockPLaceId}/consumables?page=`;
    return this.http.get<Consumable[]>(url + page);
  }

  getFiltredStockPlace(searchCriteria: string): Observable<StockPlace[]>{
    return this.http.get<StockPlace[]>(`${this.restBaseUri}stock_places?name=${searchCriteria}`);
  }

}
