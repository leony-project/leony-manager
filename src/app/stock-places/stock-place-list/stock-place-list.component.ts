import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Consumable } from '../../models/consumable';
import { ConsumablesService } from '../../consumables/consumables.service';
import { Router } from '@angular/router';
import { AuthService } from '../../security/auth.service';
import { EquipmentsService } from '../../equipments/equipments.service';
import { AlertService } from '../../components/alert/alerts.service';
import { StockPlaceService } from '../stock-place.service';
import { StockPlace } from '../../models/stockPlace';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-stock-place-list',
  templateUrl: './stock-place-list.component.html',
  styleUrls: ['./stock-place-list.component.scss']
})
export class StockPlaceListComponent implements OnInit {

  restBaseUri = environment.base_endpoint;
  elements: StockPlace[];
  @Input() param: false;
  @Input() id: string;

  private itemsNumberPerPage = 10;
  public page: number = 1;
  public pages: Array<number> = new Array<0>();

  constructor(
    public translator: TranslatorService,
    private router: Router,
    public auth: AuthService,
    private alertService: AlertService,
    private stockPlaceService: StockPlaceService
  ) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this.stockPlaceService.getAll(this.page).subscribe(data => {
      this.pages = Array.from(Array(Math.ceil(data['totalItems'] / this.itemsNumberPerPage)).keys());
      this.elements = data;
    });
  }

  onDeleteClick(element: StockPlace): void {
    //building delete msg
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('stock-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.stockPlaceService
        .toggleStockPlaceEnabled(element.id)
        .subscribe(() => {
          this.loadData();
        });
    }
  }

  updatePage(n) {
    this.page = n;
    this.loadData();
  }

}
