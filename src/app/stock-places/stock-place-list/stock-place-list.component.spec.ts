import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockPlaceListComponent } from './stock-place-list.component';

describe('StockPlaceListComponent', () => {
  let component: StockPlaceListComponent;
  let fixture: ComponentFixture<StockPlaceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockPlaceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockPlaceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
