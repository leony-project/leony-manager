import { TestBed } from '@angular/core/testing';

import { StockPlaceService } from './stock-place.service';

describe('StockPlaceService', () => {
  let service: StockPlaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockPlaceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
