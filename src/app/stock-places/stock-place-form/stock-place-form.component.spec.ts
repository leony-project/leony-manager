import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockPlaceFormComponent } from './stock-place-form.component';

describe('StockPlaceFormComponent', () => {
  let component: StockPlaceFormComponent;
  let fixture: ComponentFixture<StockPlaceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockPlaceFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockPlaceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
