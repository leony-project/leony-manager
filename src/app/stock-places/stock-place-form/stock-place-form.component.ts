import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Consumable } from "../../models/consumable";
import { ActivatedRoute, Router } from "@angular/router";
import { StockPlaceService } from "../stock-place.service";
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-stock-place-form',
  templateUrl: './stock-place-form.component.html',
  styleUrls: ['./stock-place-form.component.scss', '../../../form.scss']
})
export class StockPlaceFormComponent implements OnInit {

  stockPlaceForm: FormGroup;
  errors = [];
  isEditForm = false;
  editFormID: string;

  constructor(
    public translator: TranslatorService,
    private fb: FormBuilder,
    private stockPlaceService: StockPlaceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.stockPlaceForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)])
    });

    this.activatedRoute.paramMap.subscribe(params => {
      this.editFormID = params.get('id');

      if (this.editFormID) {
        this.isEditForm = true;
        this.stockPlaceService
          .getOne(this.editFormID)
          .subscribe((data: any) => {
            this.stockPlaceForm = this.fb.group({
              name: new FormControl(data.name, [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
            });
          });
      }
    });
  }

  submit(): any {
    if (!this.isEditForm) {
      this.create();
    } else {
      this.update();
    }
  }

  create(): any {
    const consumable = this.stockPlaceForm.value;

    if (this.stockPlaceForm.valid) {
      this.stockPlaceService
        .createStockPlace(consumable)
        .subscribe((data) => {
          if (data.id) {
            this.router
              .navigate(['/stock-place'])
              .then();
          } else {
            this.errors.push('Invalid data received');
          }
        }, (error) => {
          this.errors.push(error.message);
        });
    }
  }

  update(): any {
    const consumable = this.stockPlaceForm.value;

    if (this.stockPlaceForm.valid) {
      this.stockPlaceService
        .patchStockPlace(this.editFormID, consumable)
        .subscribe((data) => {
          if (data.id) {
            this.router
              .navigate(['/stock-place', this.editFormID])
              .then();
          } else {
            this.errors.push('Invalid data received');
          }
        }, (error) => {
          this.errors.push(error.message);
        });
    }
  }

}
