import { Component, OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { LoginService } from "./login.service";
import { Router } from "@angular/router";
import {User} from "../models/user";
import {AuthService} from '../security/auth.service';
import { TranslatorService } from '../translator/translator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm;

  constructor(
    private formBuilder: FormBuilder,
    private service: LoginService,
    private authService: AuthService,
    public translator: TranslatorService,
    private router: Router
  ) {
    if(localStorage.getItem('token'))
      this.router.navigate(['./']);

    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    })
  }

  ngOnInit() {
    $('#wrap-content').css('padding-top', 0).css('padding-left', 0);
  }

  login() {
    let email = this.loginForm.controls.email.value;
    let password = this.loginForm.controls.password.value;
    $('.loader').show();
    $('#error-message').hide();

    this.service.login(email, password).subscribe((data) => {
      $('.loader').hide();
      if(data['token']) {
        localStorage.setItem('token', data['token']);
        this.authService.getCurrentUser().subscribe((user: User) => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.router.navigate(['./']).then(() => { window.location.reload(); });
        });
      } else {
        $('#error-message').show();
      }
    }, () => {
      $('.loader').hide();
      $('#error-message').show();
    });
  }
}
