import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiURL = environment.rest_endpoint;

  constructor(private http: HttpClient) { }

  login(email, password) {
    let url = this.apiURL + 'authentication_token';
    return this.http.post(url,{ email: email, password: password });
  }
}
