import { Component, OnInit } from '@angular/core';
import { LendingsService } from '../lendings/lendings.service';
import { Lending } from '../models/lending';
import { AuthService } from '../security/auth.service';
import { Equipment } from '../models/equipment';
import { EquipmentsService } from '../equipments/equipments.service';
import { environment } from '../../environments/environment';
import { UsersService } from '../users/users.service';
import { User } from '../models/user';
import { faCube, faScrewdriver } from '@fortawesome/free-solid-svg-icons';
import { ConsumablesService } from '../consumables/consumables.service';
import { Consumable } from '../models/consumable';
import { Observable } from 'rxjs';
import { TranslatorService } from '../translator/translator.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  public lendingsNotReturned: Observable<Lending[]>
  public lendingsNotAccepted: Observable<Lending[]>
  public equipments: Observable<Equipment[]>;
  public consumables: Observable<Consumable[]>;
  public restBaseUri = environment.base_endpoint;
  public users: Observable<User[]>;
  faCube = faCube;
  faScrewdriver = faScrewdriver;

  constructor(
    public translator: TranslatorService,
    public lendingsService: LendingsService,
    private equipmentService: EquipmentsService,
    public authService: AuthService,
    public userService: UsersService,
    public consumableService: ConsumablesService,
  ) { }

  ngOnInit(): void {
    this.loadLendings();
    this.loadEquipments();
    this.loadUsers();
    this.loadConsumables();
  }

  loadLendings(): void {
    this.lendingsNotReturned = this.lendingsService.getAllNotReturned();
    this.lendingsNotAccepted = this.lendingsService.getAllLendingsRequest();
  }

  loadUsers(): void {
    this.users = this.userService.getAll();
  }

  loadEquipments(): void {
    this.equipments = this.equipmentService.getAll();
  }

  loadConsumables(): void {
    this.consumables = this.consumableService.getAll();
  }

  delete(user: User): void{
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('user-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.userService.toggleUserEnabled(user).subscribe(() => { this.loadUsers() });
    }
  }
}
