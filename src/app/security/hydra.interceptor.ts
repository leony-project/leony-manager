import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";

@Injectable()
export class HydraInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if(event.body && event.body['hydra:member']) {
          let body = event.body['hydra:member'];
          body.totalItems = event.body['hydra:totalItems']
          return event.clone({
            body: body
          });
        }
      }

      return event;
    }));
  }
}
