import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { User } from "../models/user";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiURL = environment.rest_endpoint;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  public get currentUserValue(): User {
    return JSON.parse(localStorage.getItem('currentUser'))
  }

  getCurrentUser() {
    return this.http.get<User>(this.apiURL + 'users/get/me');
  }

  isConnected() {
    return localStorage.getItem('token');
  }

  isAdmin() {
    if (this.isConnected() && this.currentUserValue !== null) return this.currentUserValue.isAdmin;
    return false;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
  }
}
