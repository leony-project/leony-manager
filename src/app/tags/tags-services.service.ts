import { Equipment } from './../models/equipment';
import { Consumable } from './../models/consumable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Tag } from '../models/tag';
import { cpuUsage } from 'process';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TagsServices {

  restBaseUri = environment.rest_endpoint;

  patchHeaders = {
    'Content-Type': 'application/merge-patch+json',
    Accept: 'application/json'
  };

  constructor(private http: HttpClient) { }


  getAllTags(page = 1): Observable<Tag[]> {
    const url = `${this.restBaseUri}tags?page=`;
    return this.http.get<Tag[]>(url + page);
  }

  getAllEquipementsTag(tagId, page = 1): Observable<Equipment[]> {
    const url = `${this.restBaseUri}tags/${tagId}/equipments?page=`;
    return this.http.get<Equipment[]>(url + page);
  }
  getAllConsumablesTag(tagId, page = 1): Observable<Consumable[]> {
    const url = `${this.restBaseUri}tags/${tagId}/consumables?page=`;
    return this.http.get<Consumable[]>(url + page);
  }

  deleteTag(tagId) {
    return this.http.delete(this.restBaseUri + 'tags/' + tagId);
  }
  
  toggleTagEnabled(tagId): Observable<Tag> {
    return this.http.post<Tag>(`${this.restBaseUri}tags/${tagId}/toggle_enabled`, {});
  }
  
  update(params: any, tagId: string) {
    const url = `${this.restBaseUri}tags/${tagId}`;
    return this.http.patch<any>(url, params, { headers: this.patchHeaders });
  }

  add(params: any) {
    const url = `${this.restBaseUri}tags`;
    params.enabled = true;
    return this.http.post<Tag>(url, params);
  }



}
