import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { Tag } from 'src/app/models/tag';
import { TranslatorService } from 'src/app/translator/translator.service';
import { TagsServices } from '../tags-services.service';

@Component({
  selector: 'app-tag-input',
  templateUrl: './tag-input.component.html',
  styleUrls: ['./tag-input.component.scss']
})
export class TagInputComponent implements OnInit {

  public newTag: Tag = <Tag>{};

  constructor(private alertService: AlertService,
    public translator: TranslatorService,
    private tagsService: TagsServices) { }

  ngOnInit(): void {
  }

  add(newTag: string) {
    let request = {
      name: "",
      color: "",
      consumables: [],
      equipments: []
    };
    if (newTag != "") {
      request.name = newTag;
      var val = Math.floor(1000 + Math.random() * 9000);
      request.color = '#' + val;
      this.tagsService.add(request).subscribe((data) => {
        if (data.id) {
          this.alertService.success(this.translator.getSentence("add-tag"))
        }
        else {
          this.alertService.error(this.translator.getSentence("invalid-data"))
        }
      }, (error) => {
        this.alertService.error(error.message);
      });
    }
    else
      this.alertService.error(this.translator.getSentence('invalid-form'));

  }
}
