import { TestBed } from '@angular/core/testing';

import { TagsServices } from './tags-services.service';

describe('TagsServices', () => {
  let service: TagsServices;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TagsServices);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
