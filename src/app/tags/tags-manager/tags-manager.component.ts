import { Equipment } from './../../models/equipment';
import { Entrance } from './../../models/entrance';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/security/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { Tag } from 'src/app/models/tag';
import { stringify } from '@angular/compiler/src/util';
import { Observable } from 'rxjs';
import { Consumable } from 'src/app/models/consumable';
import { TagsServices } from '../tags-services.service';
import { TranslatorService } from 'src/app/translator/translator.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tags-manager',
  templateUrl: './tags-manager.component.html',
  styleUrls: ['./tags-manager.component.scss']
})
export class TagsManagerComponent implements OnInit {
  public tagsList: Tag[];
  public current: Tag = <Tag>{};
  public newTag: Tag = <Tag>{};
  equipementList: Equipment[];
  public consumableList: Consumable[];
  public allTagsCE: any[] = [];
  public tagsPage = 1;
  public tagsPages: Array<number> = new Array<0>();
  public equipmentsPage = 1;
  public equipmentsPages: Array<number> = new Array<0>();
  public consumablesPage = 1;
  public consumablesPages: Array<number> = new Array<0>();
  private itemsNumberPerPage = 10;
  public restBaseUri = environment.base_endpoint
  tagId: string;

  constructor(
    public translator: TranslatorService,
    public auth: AuthService,
    private router: Router,
    private alertService: AlertService,
    private tagsService: TagsServices) { }

  ngOnInit(): void {
    if (!this.auth.isAdmin())
      this.router.navigate(['/']);
    this.init();
  }

  init() {
    this.getAllTags();
    this.newTag.color = "#000000"
  }

  getLists(tag: Tag) {
    this.tagId = tag.id
    this.setCurrentTag(tag);
    this.getEquipementList(this.tagId);
    this.getConsumableList(this.tagId)
  }


  getAllTags() {
    this.tagsService.getAllTags(this.tagsPage).subscribe((tags: Tag[]) => {
      this.tagsPages = Array.from(Array(Math.ceil(tags['totalItems'] / this.itemsNumberPerPage)).keys());
      this.tagsList = tags;
      this.tagsList.forEach(element => {
        if (!element.color.includes("#"))
          element.color = "#" + element.color;
      });
    });
  }

  getEquipementList(tagId) {
    this.tagsService.getAllEquipementsTag(tagId, this.equipmentsPage).subscribe((equipments: Equipment[]) => {
      this.equipmentsPages = Array.from(Array(Math.ceil(equipments['totalItems'] / this.itemsNumberPerPage)).keys());
      this.equipementList = equipments;
    })

  }
  getConsumableList(tagId) {
    this.tagsService.getAllConsumablesTag(tagId, this.consumablesPage).subscribe((consumables: Consumable[]) => {
      this.consumablesPages = Array.from(Array(Math.ceil(consumables['totalItems'] / this.itemsNumberPerPage)).keys());
      this.consumableList = consumables;
    })
  }

  setCurrentTag(tag: Tag) {
    if (this.current)
      this.current = <Tag>{};
    this.current.id = tag.id;
    this.current.color = tag.color;
    this.current.name = tag.name;
  }

  deleteTag(tag: Tag) {
    let msg = this.translator.getSentence('ask-delete-this') + 'tag ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.tagsService.toggleTagEnabled(tag.id).subscribe(() =>{
        this.alertService.success(this.translator.getSentence("tag-deleted"));
        this.getAllTags();
      });
    }
  }

  update(current: Tag) {
    let request = {
      name: "",
      color: ""
    };
    request.name = current.name;
    request.color = current.color;
    this.tagsService.update(request, current.id).subscribe({
      next: () => {
        this.alertService.success(this.translator.getSentence('update-tag'), {
          keepAfterRouteChange: true,
        });
        this.getAllTags();
      },
      error: (error) => {
        this.alertService.error(error.message);
      },
    })
  }

  add(newTag: Tag) {
    let request = {
      name: "",
      color: "",
      consumables: [],
      equipments: []
    };
    if (newTag.name != "" && newTag.color != "") {
      request.name = newTag.name;
      request.color = newTag.color;
      this.tagsService.add(request).subscribe((data) => {
        if (data.id) {
          this.getAllTags();
        }
        else {
          this.alertService.error(this.translator.getSentence('invalid-data'))
        }
      }, (error) => {
        this.alertService.error(error.message);
      });
    }
    else
      this.alertService.error(this.translator.getSentence('invalid-form'));

  }

  updatePage(n, keyword) {
    switch (keyword) {
      case 'equipments':
        this.equipmentsPage = n;
        this.getEquipementList(this.tagId);
        break;
      case 'consumables':
        this.consumablesPage = n;
        this.getConsumableList(this.tagId);
        break;
      case 'tags':
        this.tagsPage = n;
        this.getAllTags();
        break;
    }
  }

}
