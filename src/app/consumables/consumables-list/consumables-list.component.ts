import { Component, Input, OnInit } from '@angular/core';
import { ConsumablesService } from '../consumables.service';
import { Router } from '@angular/router';
import { Consumable } from '../../models/consumable';
import { AuthService } from '../../security/auth.service';
import { EquipmentsService } from 'src/app/equipments/equipments.service';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { Equipment } from 'src/app/models/equipment';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-consumables-list',
  templateUrl: './consumables-list.component.html',
  styleUrls: ['./consumables-list.component.scss', '../../../style/card.scss']
})
export class ConsumablesListComponent implements OnInit {

  restBaseUri = environment.base_endpoint;
  consumables: Consumable[];
  equipments: Equipment[];
  consumable: Consumable = new Consumable();
  @Input() equipmentId: string;
  @Input() equipment: Equipment
  showEquipmentConsumables = false;

  public elementsPage = 1;
  public elementsPages: Array<number> = new Array<0>();
  private itemsNumberPerPage = 10;

  constructor(
    public translator: TranslatorService,
    private readonly consumablesService: ConsumablesService,
    private readonly router: Router,
    public auth: AuthService,
    private readonly equipmentService: EquipmentsService,
    private readonly alertService: AlertService,
  ) {
  }

  ngOnInit(): void {
    this.showEquipmentConsumables = !!this.equipmentId;
    if (this.showEquipmentConsumables) {
      this.loadEquipmentConsumables();
    } else {
      this.loadAllConsumables();
    }
  }

  loadAllConsumables(): void {
    this.consumablesService.getAll(this.elementsPage).subscribe(data => {
      this.elementsPages = Array.from(Array(Math.ceil(data.totalItems / this.itemsNumberPerPage)).keys());
      this.consumables = data;
    });
  }

  onDeleteClick(consumableID): void {
    
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('equipment-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.consumablesService.toggleConsumableEnabled(consumableID).subscribe(
          {
          next: () => {
            window.location.reload();
            this.alertService.success(this.translator.getSentence('consum-deleted'), {
              keepAfterRouteChange: true,
            });
            
  
          },
          error: (error) => {
            this.alertService.error(error);
          },
        })
      
    };
  }

  // Params for equipment details
  loadEquipmentConsumables(): void {
    this.equipmentService.getAllEquipmentConsumables(this.equipmentId, this.elementsPage).subscribe((data) => {
      this.elementsPages = Array.from(Array(Math.ceil(data.totalItems / this.itemsNumberPerPage)).keys());
      this.consumables = data;
    });
  }

  removeConsumable(consummableId): void {
    this.consumablesService.getOne(consummableId).subscribe((consumable: Consumable) => {
      this.consumable = consumable;
      const equipments = consumable.equipments
      const index = equipments.findIndex(x => x.id === this.equipmentId)
      if (index > -1) {
        equipments.splice(index, 1)
        const equipmentIds = equipments.map(s => s.id)
        this.consumablesService.patchConsumable(consummableId, { equipments: equipmentIds }).subscribe(
          {
            next: () => {
              this.alertService.success(this.translator.getSentence('delete-consumable'), {
                keepAfterRouteChange: true,
              });
              this.loadEquipmentConsumables();
            },
            error: (error) => {
              this.alertService.error(error);
            },
          });
      }

    });
  }

  addConsumable(id: string): void {
    this.router.navigate(['/equipments', id, 'addConsumable']);
  }

  updatePage(n): void {
    this.elementsPage = n;
    if (this.showEquipmentConsumables) {
      this.loadEquipmentConsumables();
    } else {
      this.loadAllConsumables();
    }
  }
}
