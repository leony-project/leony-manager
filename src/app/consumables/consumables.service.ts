import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Consumable} from '../models/consumable';
import {Observable} from 'rxjs';
import {FormGroup} from "@angular/forms";
import { AlertService } from '../components/alert/alerts.service';
import { Router } from '@angular/router';
import { TranslatorService } from '../translator/translator.service';

@Injectable({
  providedIn: 'root'
})
export class ConsumablesService {

  restBaseUri = environment.rest_endpoint;

  patchHeaders = new HttpHeaders({
    'Content-Type': 'application/merge-patch+json',
    Accept: 'application/json'
  });

  fileHeaders = new HttpHeaders({
    Accept: 'application/json'
  });

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private router: Router,
    private translator: TranslatorService,
     ) {
  }

  getAll(page = 1): Observable<any> {
    return this.http.get<Consumable[]>(`${this.restBaseUri}consumables?page=${page}`);
  }


  getFiltredConsumable(searchCriteria: string): Observable<Consumable[]>{
    return this.http.get<Consumable[]>(`${this.restBaseUri}consumables?name=${searchCriteria}`);
  }

  getOne(consumableId): Observable<Consumable> {
    return this.http.get<Consumable>(`${this.restBaseUri}consumables/${consumableId}`);
  }

  getAllConsumables(): any {
    const url = this.restBaseUri + 'consumables';
    return this.http.get<Consumable[]>(url);
  }

  createConsumable(consumableParams : Consumable): Observable<Consumable> {
    
    return this.http.post<Consumable>(`${this.restBaseUri}consumables`, consumableParams);
  }

  postFile(form: FormGroup): any {
    const url = this.restBaseUri + 'media_objects';
    const formData = new FormData();
    formData.append('file', form.get('file').value);

    return this.http.post<any>(url, formData, {headers: this.fileHeaders});
  }

  toggleConsumableEnabled(consumableId): Observable<Consumable> {
    return this.http.post<Consumable>(`${this.restBaseUri}consumables/${consumableId}/toggle_enabled`, {});
  }

  patchConsumable(id, changePayload) {
    return this.http.patch<Consumable>(`${this.restBaseUri}consumables/${id}`, changePayload, {headers: this.patchHeaders});
  }

  deleteItem(consumableID): Observable<HttpResponse<any>> {
    return this.http.delete(`${this.restBaseUri}consumables/${consumableID}`, {observe: 'response'});
  }

  onDeleteClick(consumableID, test = null): void {
    this.deleteConsumable(consumableID).subscribe(
      {
        next: () => {
          this.alertService.success(this.translator.getSentence('delete-consumable'), {
            keepAfterRouteChange: true,
          });
        this.router.navigate(['/consumables'])
        if(test != null){
          test;
        }
        },
        error: (error) => {
          this.alertService.error(error);
          console.log(error)
        },
      });
  }

  deleteConsumable(consumableID) {
    return this.http.delete(this.restBaseUri + `consumables/${consumableID}`);
  }

}
