import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumablesDetailsComponent } from './consumables-details.component';

describe('ConsumablesDetailsComponent', () => {
  let component: ConsumablesDetailsComponent;
  let fixture: ComponentFixture<ConsumablesDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsumablesDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumablesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
