import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConsumablesService } from '../consumables.service';
import { Consumable } from '../../models/consumable';
import { FormControl, Validators } from '@angular/forms';
import { Tag } from '../../models/tag';
import { environment } from '../../../environments/environment';
import { AuthService } from 'src/app/security/auth.service';
import { InOutConsumablesService } from 'src/app/in-out-consumables/in-out-consumables.service';
import { Entrance } from 'src/app/models/entrance';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { TagsServices } from 'src/app/tags/tags-services.service';
import { Observable } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-consumables-details',
  templateUrl: './consumables-details.component.html',
  styleUrls: ['./consumables-details.component.scss']
})
export class ConsumablesDetailsComponent implements OnInit {

  restBaseUri = environment.base_endpoint;
  consumable: Consumable = new Consumable();
  quantity: number;
  newEntrance: Entrance;

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(
    public translator: TranslatorService,
    private readonly consumablesService: ConsumablesService,
    private readonly tagsService: TagsServices,
    private readonly router: Router,
    private readonly alertService: AlertService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly inOutConsumablesServices: InOutConsumablesService,
    public auth: AuthService,
  ) {
  }

  ngOnInit(): void {

    this.loadConsumable();
    this.loadTags();

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  loadConsumable() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.consumable.id = params.get('id');
      this.consumablesService.getOne(this.consumable.id).subscribe((data: Consumable) => {
        this.selectedItems = data.tags.map(e => ({ item_id: e.id, item_text: e.name }));
        this.consumable = data;
      });
    });
  }

  loadTags() {
    this.tagsService
      .getAllTags()
      .subscribe((data: Tag[]) => {
        this.dropdownList = data.map(e => ({ item_id: e.id, item_text: e.name }));
      });
  }

  onItemSelect(item: any): void {
    this.updateTags();
  }

  onSelectAll(items: any): void {
    this.updateTags(items);
  }

  updateTags(items?: Array<any>): void {
    const payload = {
      tags: items ? items.map(e => e.item_id) : this.selectedItems.map(e => e.item_id)
    };

    this.consumablesService
      .patchConsumable(this.consumable.id, payload)
      .subscribe((consumable: Consumable) => {
        this.consumable.tags = consumable.tags;
      });
  }

  onEnabledChange(event): void {
    if (this.auth.isAdmin) {

      this.consumablesService.toggleConsumableEnabled(this.consumable.id).subscribe((consumable: Consumable) => {
        this.consumable.enabled = consumable.enabled;
      });
    }
  }

  onDeleteClick(id: string): void {
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('consumable-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.consumablesService
        .toggleConsumableEnabled(id)
        .subscribe(
          {
          next: () => {
            this.router.navigate(['/consumables']).then(() => {
              window.location.reload();
            });
            this.alertService.success(this.translator.getSentence('consum-deleted'), {
              keepAfterRouteChange: true,
            });
  
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });
    }
  }

  buildEntrance(entrance: boolean): void {
    this.newEntrance = {
      quantity: this.quantity,
      consumable: this.consumable.id,
      date: new Date(),
      entrance: entrance,
      enabled : true
    };
  }

  inRequest(): void {
    this.intOutRequest(true);
  }

  outRequest(): void {
    this.intOutRequest(false);
  }

  intOutRequest(incoming: boolean): void {
    this.buildEntrance(incoming);
    if (this.newEntrance.quantity && this.newEntrance.quantity > 0) {
      this.inOutConsumablesServices.addInOutConsumable(this.newEntrance).subscribe((data) => {
        if (data.id) {
          this.alertService.success(this.translator.getSentence("add-entrance"));
        } else {
          this.alertService.error(this.translator.getSentence('error-add-entrance'));
        }
      }, (error) => {
        console.error(error);
      });
    }
  }

}

