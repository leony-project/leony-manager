import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConsumablesService } from '../consumables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Consumable } from '../../models/consumable';
import { Tag } from '../../models/tag';
import { StockPlaceService } from '../../stock-places/stock-place.service';
import { StockPlace } from '../../models/stockPlace';
import bsCustomFileInput from 'bs-custom-file-input';
import { TagsServices } from 'src/app/tags/tags-services.service';
import { Observable, Subject } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-add-consumables',
  templateUrl: './add-consumables.component.html',
  styleUrls: ['./add-consumables.component.scss']
})
export class AddConsumablesComponent implements OnInit {

  stockPlaces: StockPlace[] = [];
  public stockPlacesTypeahead = new Subject<string>();

  consumableForm: FormGroup;
  fileUploadForm: FormGroup;
  errors = [];
  isEditForm = false;
  ConsumableId: string;

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(
    public translator: TranslatorService,
    private fb: FormBuilder,
    private consumablesService: ConsumablesService,
    private tagsService: TagsServices,
    private stockPlaceService: StockPlaceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {

    bsCustomFileInput.init()

    this.loadStockPlaces();
    this.initFormBuilder();
    this.loadTags();

    this.reloadConsumableData();


    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  private loadStockPlaces(): void {
    this.filterStockPlaces('');
    this.stockPlacesTypeahead.subscribe((term) => {
      this.filterStockPlaces(term);
    });
  }

  private filterStockPlaces(term): void {
    this.stockPlaceService.getFiltredStockPlace(term).subscribe((data) => {
      this.stockPlaces = data;
    });
  }

  initFormBuilder() {
    this.consumableForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      quantity: new FormControl(0, [Validators.required, Validators.min(0)]),
      unity: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      tags: new FormControl([], []),
      enabled: new FormControl(true, Validators.required),
      image: new FormControl('', [Validators.required]),
      stockPlace: new FormControl(undefined, [Validators.required]),
      description: new FormControl(''),
    });

    this.fileUploadForm = this.fb.group({
      file: ['']
    });
  }

  loadTags() {
    this.tagsService
      .getAllTags()
      .subscribe((data: Tag[]) => {
        this.dropdownList = data.map(e => ({ item_id: e.id, item_text: e.name }));
      });

  }

  reloadConsumableData() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.ConsumableId = params.get('id');

      if (this.ConsumableId) {
        this.isEditForm = true;
        this.consumablesService
          .getOne(this.ConsumableId)
          .subscribe((data: Consumable) => {
            this.consumableForm = this.fb.group({
              name: new FormControl(data.name, [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
              quantity: new FormControl(data.quantity, [Validators.required, Validators.min(0)]),
              unity: new FormControl(data.unity, [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
              tags: new FormControl(data.tags.map(e => ({ item_id: e.id, item_text: e.name })), []),
              image: new FormControl(data.image ? data.image.id : undefined, []),
              stockPlace: new FormControl(data.stockPlace.id, [Validators.required]),
              description: new FormControl(data.description),
            });

            this.selectedItems = data.tags.map(e => ({ item_id: e.id, item_text: e.name }));
          });
      }
    });
  }

  submit(): any {
    this.consumableForm.get('tags').setValue(this.selectedItems.map(e => e.item_id));

    if (!this.isEditForm) {
      this.create();
    } else {
      this.update();
    }
  }

  create(): any {
    const consumable = this.consumableForm.value;

    if (this.consumableForm.valid) {
      this.consumablesService
        .createConsumable(consumable)
        .subscribe((data) => {
          if (data.id) {
            this.router
              .navigate(['/consumables'])
              .then();
          } else {
            this.errors.push('Invalid data received');
          }
        }, (error) => {
          this.errors.push(error.message);
        });
    }
  }

  update(): any {
    const consumable = this.consumableForm.value;

    if (this.consumableForm.valid) {
      this.consumablesService
        .patchConsumable(this.ConsumableId, consumable)
        .subscribe((data) => {
          if (data.id) {
            this.router
              .navigate(['/consumables', data.id])
              .then();
          } else {
            this.errors.push('Invalid data received');
          }
        }, (error) => {
          this.errors.push(error.message);
        });
    }
  }

  handleFileInput(files: FileList): void {
    this.fileUploadForm.get('file').setValue(files.item(0));

    this.consumablesService.postFile(this.fileUploadForm).subscribe(data => {
      this.consumableForm.get('image').setValue(data.id);
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

}
