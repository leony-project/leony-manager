import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddConsumablesComponent } from './add-consumables.component';

describe('AddConsumablesComponent', () => {
  let component: AddConsumablesComponent;
  let fixture: ComponentFixture<AddConsumablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddConsumablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddConsumablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
