import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { TranslatorService } from 'src/app/translator/translator.service';
import { LoggingService } from '../logging.service';
import { Logging } from 'src/app/models/logging';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss'],
})
export class LoggingComponent implements OnInit {
  logsList: Logging[];
  currentPage = 1;
  itemsPerPage = 10; // Nombre d'éléments à afficher par page
  totalItems = 0; // Nombre total d'éléments
  totalPages = 0; // Nombre total de pages
  pages: number[]; // Numéros de page

  constructor(
    private alertService: AlertService,
    public translator: TranslatorService,
    private loggingService: LoggingService
  ) {}

  ngOnInit(): void {
    this.loggingService
      .getAll(this.currentPage, this.itemsPerPage)
      .subscribe(
        ({ items, totalItems }: { items: Logging[]; totalItems: number }) => {
          this.logsList = this.formatJson(items);
          this.totalItems = totalItems;
          this.totalPages = Math.ceil(this.totalItems / this.itemsPerPage);
          this.pages = Array.from(Array(this.totalPages)).map((x, i) => i + 1);
        }
      );
  }

  // Méthode appelée lorsque l'utilisateur clique sur le bouton "Précédent"
  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.loggingService
        .getAll(this.currentPage, this.itemsPerPage)
        .subscribe(({ items, totalItems }: { items: Logging[]; totalItems: number }) => {
          this.logsList = this.formatJson(items);
        });
    }
  }

  // Méthode appelée lorsque l'utilisateur clique sur le bouton "Suivant"
  nextPage() {
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.loggingService
        .getAll(this.currentPage, this.itemsPerPage)
        .subscribe(({ items, totalItems }: { items: Logging[]; totalItems: number }) => {
          this.logsList = this.formatJson(items);
          console.log( this.logsList);
        });
    }
  }

  // Méthode appelée lorsque l'utilisateur clique sur un numéro de page
  changePage(page: number) {
    this.currentPage = page;
    this.loggingService
      .getAll(this.currentPage, this.itemsPerPage)
      .subscribe(({ items, totalItems }: { items: Logging[]; totalItems: number }) => {
        this.logsList = this.formatJson(items);
      });
  }

  formatJson(data) {
    type Log = {
      id?: number;
      route?: string;
      uri?: string;
      type?: string;
      content?: string;
      user?: string;
    };

    let logs = [];
    data.forEach((e) => {
      let data = JSON.parse(e.data);
      const log: Log = {};
      log.id = e.id;
      log.route = data._route;
      log.uri = data._api_normalization_context?.request_uri;
      log.type = data._api_collection_operation_name !== undefined ? data._api_collection_operation_name : (data._api_item_operation_name !== undefined ? data._api_item_operation_name : "-");
      log.content = JSON.stringify(data.data);
      log.user = (e.user !== null ? e.user.email : 'Anonyme') ;
      console.log(log)
      logs.push(log);
    });
    return logs;
  }
}
