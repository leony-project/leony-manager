import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Logging } from '../models/logging';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { AlertService } from '../components/alert/alerts.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoggingService {
  restBaseUri = environment.rest_endpoint;

  patchHeaders = new HttpHeaders({
    'Content-Type': 'application/merge-patch+json',
    Accept: 'application/json',
  });

  fileHeaders = new HttpHeaders({
    Accept: 'application/json',
  });

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private router: Router
  ) {}

  getAll(
    page: number,
    itemsPerPage: number
  ): Observable<{ items: Logging[]; totalItems: number }> {
    return this.http
      .get<any>(
        this.restBaseUri +
          'loggings?page=' +
          page +
          '&itemsPerPage=' +
          itemsPerPage
      )
      .pipe(
        map((response) => ({
          items: response,
          totalItems: response.totalItems,
        }))
      );
  }

  getFiltredConsumable(searchCriteria: string): Observable<Logging[]> {
    return this.http.get<Logging[]>(
      `${this.restBaseUri}loggings?name=${searchCriteria}`
    );
  }

  getOne(consumableId): Observable<Logging> {
    return this.http.get<Logging>(
      `${this.restBaseUri}loggings/${consumableId}`
    );
  }

  getAllLogs(): any {
    const url = this.restBaseUri + 'loggings';
    return this.http.get<Logging[]>(url);
  }
}
