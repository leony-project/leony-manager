import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/security/auth.service';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-left-navbar',
  templateUrl: './left-navbar.component.html',
  styleUrls: ['./left-navbar.component.scss']
})
export class LeftNavbarComponent implements OnInit {



  constructor(public translator: TranslatorService, public auth: AuthService, private el: ElementRef, public router: Router) {
  }

  ngOnInit(): void {
  }

  closeBurgerMenu(e) {
    let leftNav = this.el.nativeElement.querySelector('#left-navbar');
    leftNav.classList.remove('opened');
  }

}
