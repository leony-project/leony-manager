import { Router } from '@angular/router';
import { Component, OnInit, ElementRef } from '@angular/core';
import { AuthService } from "../security/auth.service";
import { faColumns, faCube, faScrewdriver, faKey, faUsers, faTags, faExchangeAlt, faSearch, faChevronDown, faSignOutAlt, faMap } from '@fortawesome/free-solid-svg-icons';
import { TranslatorService } from '../translator/translator.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  faColumns = faColumns;
  faCube = faCube;
  faScrewdriver = faScrewdriver;
  faKey = faKey;
  faUsers = faUsers;
  faTags = faTags;
  faExchangeAlt = faExchangeAlt;
  faSearch = faSearch;
  faChevronDown = faChevronDown;
  faSignOutAlt = faSignOutAlt;
  faMap = faMap;
  searchCriteria: string = "";


  constructor(public translator: TranslatorService,
    public authService: AuthService,
    private router: Router, private el: ElementRef) { }

  ngOnInit(): void { }

  openContent(e) {
    let leftNav = this.el.nativeElement.querySelector('#left-navbar');
    leftNav.classList.toggle('opened');
  }

  search() {
    if (this.searchCriteria != null && this.searchCriteria != "") {
      this.router.navigate(['search', this.searchCriteria]);
    }
    this.searchCriteria = "";

  }

  logout() {
    this.authService.logout()
    this.router.navigate([''])
  }
}
