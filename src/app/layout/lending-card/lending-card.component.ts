import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LendingsService } from 'src/app/lendings/lendings.service';
import { Lending } from 'src/app/models/lending';
import { TranslatorService } from 'src/app/translator/translator.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-lending-card',
  templateUrl: './lending-card.component.html',
  styleUrls: ['./lending-card.component.scss']
})
export class LendingCardComponent implements OnInit {
  restBaseUri = environment.base_endpoint;
  @Input() lending: Lending;
  text = " Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsum quasi laborum totam earum amet neque accusantium modi qui nemo, eum quisquam voluptates dolore ea voluptatibus aspernatur. Nemo quidem explicabo exercitationem!Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsum quasi laborum totam earum amet neque accusantium modi qui nemo, eum quisquam voluptates dolore ea voluptatibus aspernatur. Nemo quidem explicabo exercitationem! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsum quasi laborum totam earum amet neque accusantium modi qui nemo, eum quisquam voluptates dolore ea voluptatibus aspernatur. Nemo quidem explicabo exercitationem!"
  constructor(
    public translator: TranslatorService,
    public lendingsService: LendingsService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  updateLendingRequest(lending: Lending) {
    if (!lending.accepted) {
      this.lendingsService.acceptLendingRequest(lending.id).subscribe(() => {
        window.location.reload();
      });
    } else if (lending.accepted) {
      this.router.navigate(['lendings/' + this.lending.id]).then(() => { window.location.reload(); });
    }

  }

  deleteLending(lendingId: string) {
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('lending-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.lendingsService.toggleLendingsEnabled(lendingId).subscribe(() => {
        window.location.reload();
      });
    }
  }
}
