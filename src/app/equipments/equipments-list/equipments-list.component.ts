import { Component, Input, OnInit } from '@angular/core';
import { EquipmentsService } from '../equipments.service';
import { Equipment } from 'src/app/models/equipment';
import { AuthService } from 'src/app/security/auth.service';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { environment } from 'src/environments/environment';
import { ConsumablesService } from '../../consumables/consumables.service';
import { Consumable } from 'src/app/models/consumable';
import { TranslatorService } from 'src/app/translator/translator.service';


@Component({
  selector: 'app-equipments-list',
  templateUrl: './equipments-list.component.html',
  styleUrls: ['./equipments-list.component.scss', '../../../style/card.scss']
})

export class EquipmentsListComponent implements OnInit {

  restBaseUri = environment.base_endpoint;
  private itemsNumberPerPage = 10;

  equipments: Equipment[];
  public equipmentPage = 1;
  public equipmentPages: Array<number> = new Array<0>();
  showEquipmentConsumablesOnly = false;

  @Input() consumableId: string; // Equipment id to fetch linked consumables only.

  constructor(
    public translator: TranslatorService,
    public equipmentService: EquipmentsService,
    private consumableService: ConsumablesService,
    public auth: AuthService,
    private alertService: AlertService,
  ) { }

  ngOnInit(): void {
    this.showEquipmentConsumablesOnly = !!this.consumableId;
    if (!this.showEquipmentConsumablesOnly) {
      this.loadEquipments();
    } else {
      this.loadEquipmentsFromConsumables();
    }
  }

  loadEquipments(): void {
    this.equipmentService.getAll(this.equipmentPage).subscribe((equipments: Equipment[]) => {
      this.equipmentPages = Array.from(Array(Math.ceil(equipments['totalItems'] / this.itemsNumberPerPage)).keys());
      this.equipments = equipments;
    });
  }

  loadEquipmentsFromConsumables(): void {
    this.consumableService.getOne(this.consumableId).subscribe(data => {
      this.equipments = data.equipments;
    });
  }

  onDeleteClick(equipmentId): void {
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('equipment-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.equipmentService.onDeleteClick(equipmentId)
    }
  }

  updatePage(n): void {
    this.equipmentPage = n;
    this.loadEquipments();
  }

  removeConsumable(equipmentId): void {

    const index = this.equipments.findIndex(x => x.id === equipmentId)
    if (index > -1) {
      this.equipments.splice(index, 1);
      const equipmentIds = this.equipments.map(s => s.id)
      this.consumableService.patchConsumable(this.consumableId, { equipments: equipmentIds }).subscribe(
        {
          next: () => {
            this.alertService.success(this.translator.getSentence('delete-equipment'), {
              keepAfterRouteChange: true,
            });
            this.loadEquipmentsFromConsumables();
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });
    }


  }

}
