import { Component, OnInit } from '@angular/core';
import { EquipmentsService } from '../equipments.service';
import { Entity } from 'src/app/models/entity';
import { Equipment } from 'src/app/models/equipment';
import { ActivatedRoute } from '@angular/router';
import { TagsServices } from 'src/app/tags/tags-services.service';
import { Tag } from 'src/app/models/tag';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/security/auth.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { EntityState } from 'src/app/models/EntityState.enum';
import { StockPlaceService } from 'src/app/stock-places/stock-place.service';
import { Observable } from 'rxjs';
import { StockPlace } from 'src/app/models/stockPlace';
import { TranslatorService } from 'src/app/translator/translator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-equipment-details',
  templateUrl: './equipment-details.component.html',
  styleUrls: ['./equipment-details.component.scss']
})

export class EquipmentDetailsComponent implements OnInit {

  restBaseUri = environment.base_endpoint;
  equipment: Equipment = new Equipment();
  equipmentEntities: Entity[];
  entitiesLength: number
  reference: string
  entityForm
  stockPlace: Observable<StockPlace>

  public entityPage = 1;
  public entityPages: Array<number> = new Array<0>();
  private itemsNumberPerPage = 10;


  // For tags
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(
    public translator: TranslatorService,
    private equipmentService: EquipmentsService,
    private tagsService: TagsServices,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    public auth: AuthService,
    public formBuilder: FormBuilder,
    public stockPlaceService: StockPlaceService,
    public router: Router

  ) { }

  ngOnInit(): void {

    this.loadEquipment();
    this.loadEntities();

    this.entityForm = this.formBuilder.group({
      entititiesNumber: new FormControl('', [Validators.required,]),
    });

    this.tagsService.getAllTags().subscribe((data: Tag[]) => {
      this.dropdownList = data.map(tag => ({ item_id: tag.id, item_text: tag.name }));
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true
    };

  }

  loadEquipment() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.equipment.id = params.get('id');
      
      this.equipmentService.getOne(this.equipment.id).subscribe({
        next : (data: Equipment) => {
          this.equipment = data;
          this.equipment.stockPlace;
          this.loadStockPlace(this.equipment.stockPlace.id)
          this.selectedItems = data.tags.map(tag => ({ item_id: tag.id, item_text: tag.name }));
        },
        error: (error)=> {
          this.router.navigate(['**']);
        }
      });
    });
  }

  loadTags() {
    this.tagsService.getAllTags().subscribe((data: Tag[]) => {
      this.dropdownList = data.map(tag => ({ item_id: tag.id, item_text: tag.name }));
    });
  }

  loadStockPlace(id) {
    this.stockPlace = this.stockPlaceService.getOne(id)
  }

  // Patch Equipment "enable"
  onEnabledChange(event) {
    if (this.auth.isAdmin) {
      this.equipmentService.toggleEquipmentEnabled(this.equipment.id).subscribe((equipment: Equipment) => {
        this.equipment.enabled = equipment.enabled
      })
    }
  }

  // Tags
  updateTags(): void {
    const payload = {
      tags: this.selectedItems.map(tag => tag.item_id)
    };
    this.equipmentService.patchEquipment(this.equipment.id, payload).subscribe((equipment: Equipment) => {
      this.equipment.tags = equipment.tags;
    });
  }

  // Equipment Entities
  loadEntities() {
    this.equipmentService.getAllEquipmentsEntities(this.equipment.id, this.entityPage).subscribe((entities) => {
      this.entityPages = Array.from(Array(Math.ceil(entities['totalItems'] / this.itemsNumberPerPage)).keys());
      this.equipmentEntities = entities;
      var entity = entities.length;
      this.entitiesLength = ((entities.length > 0 ? entity : 0) + 1);
    });
  }

  deleteEntity(id) {
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('entity-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.equipmentService.toggleEquipmentsEntitiesEnabled(id).subscribe(
        {
          next: () => {
            this.alertService.success(this.translator.getSentence('remove-entity'), {
              keepAfterRouteChange: true,
            });
            this.loadEntities();
            console.log('fvr')
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });

    }
  }

  generateEntities() {
    const generateEntitiesForm = this.entityForm.value;
    this.equipmentService.generateEntities(generateEntitiesForm.entititiesNumber, this.equipment.id, this.entitiesLength);
    // .subscribe({
    // next: () => {
    this.alertService.success(this.translator.getSentence('add-entity'), {});
    this.loadEntities();

    // },
    // error: (error) =>  {
    // this.alertService.error(error);
    // }
    // });

  }



  onDeleteClick(equipmentId){
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('equipment-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.equipmentService.onDeleteClick(equipmentId);
    }
  }

  updatePage(n): void {
    this.entityPage = n;
    this.loadEntities();
  }

}





