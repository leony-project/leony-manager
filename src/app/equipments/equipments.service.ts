import { Equipment } from 'src/app/models/equipment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Entity } from '../models/entity';
import { Consumable } from '../models/consumable';
import {FormGroup} from "@angular/forms";
import { Observable } from 'rxjs';
import { EntityState } from '../models/EntityState.enum';
import { AlertService } from '../components/alert/alerts.service';
import { Router } from '@angular/router';
import { TranslatorService } from '../translator/translator.service';


@Injectable({
  providedIn: 'root'
})

export class EquipmentsService {

  restBaseUri = environment.rest_endpoint;

  patchHeaders = new HttpHeaders({
    'Content-Type': 'application/merge-patch+json',
    Accept: 'application/json'
  });

  fileHeaders = new HttpHeaders({
    Accept: 'application/json'
  });


  constructor(
    private translator: TranslatorService,
    private http: HttpClient,
    private alertService: AlertService,
    private router: Router,
     ) {
  }

  // Equipments
  getAll(page = 1): Observable<any>  {
    return this.http.get<Equipment[]>(this.restBaseUri + 'equipment?page=' + page);
  }

  getFiltredEquipment(searchCriteria: string): Observable<Equipment[]> {
    return this.http.get<Equipment[]>(this.restBaseUri + 'equipment?name=' + searchCriteria);
  }

  getOne(EquipmentId): Observable<Equipment> {
    return this.http.get<Equipment>(this.restBaseUri + 'equipment/' + EquipmentId);
  }

  addEquipment(params) {
    return this.http.post<Equipment>(this.restBaseUri + 'equipment', params);
  }

  toggleEquipmentEnabled(equipmentId): Observable<Equipment> {
    return this.http.post<Equipment>(`${this.restBaseUri}equipments/${equipmentId}/toggle_enabled`, {});
  }
  
  patchEquipment(equipmentId, changePayload): any {
    return this.http.patch<Equipment>(this.restBaseUri + `equipment/${equipmentId}`, changePayload, {headers: this.patchHeaders});
  }

  deleteEquipment(equipmentId) {
    return this.http.delete(this.restBaseUri + `equipment/${equipmentId}`);
  }


  // Equipment entities
  getAllEquipmentsEntities(equipmentId, page = 1): Observable<Entity[]> {
    return this.http.get<Entity[]>(this.restBaseUri + `equipment/${equipmentId}/entities?page=` + page);
  }

  addEquipmentEntity(equipmentEntity): Observable<Entity>  {
    return this.http.post<Entity>(this.restBaseUri + 'entities', equipmentEntity);
  }

  deleteEquipmentsEntities(equipmentEntityId){
    return this.http.delete(this.restBaseUri + `entities/${equipmentEntityId}`);
  }

  toggleEquipmentsEntitiesEnabled(equipmentEntityId): Observable<Equipment> {
    return this.http.post<Equipment>(`${this.restBaseUri}entities/${equipmentEntityId}/toggle_enabled`, {});
  }

  getOneEntity(entityId): Observable<Entity>  {
    return this.http.get<Entity>(this.restBaseUri + 'entities/' + entityId);
  }

  getFiltredEntity(searchCriteria: string){
    return this.http.get<Entity[]>(this.restBaseUri + 'entities?reference=' + searchCriteria)
  }

  getAllEntities(): Observable<Entity[]>  {
    return this.http.get<Entity[]>(this.restBaseUri + 'entities?enabled=true');
  }

  getNotLendedsEntities() {
    return this.http.get<Entity[]>(this.restBaseUri + 'entities/not-lendeds');
  }

  patchEntity(entityId, changePayload): any {
    return this.http.patch<Entity>(this.restBaseUri + `entities/${entityId}`, changePayload, {headers: this.patchHeaders});
  }


  // Equipment consumables
  getAllEquipmentConsumables(equipmentId, page = 1): Observable<any>{
    return this.http.get<Consumable[]>( this.restBaseUri + `equipment/${equipmentId}/consumables?page=` + page);
  }


  // Uploadf files
  postFile(form: FormGroup): any {
    const url = this.restBaseUri + 'media_objects';
    const formData = new FormData();
    formData.append('file', form.get('file').value);

    return this.http.post<any>(url, formData, {headers: this.fileHeaders});
  }


  loadEntities(equipmentId) {
    console.log(equipmentId)
    this.getAllEquipmentsEntities(equipmentId).subscribe((entities) => {
      console.log(entities)
      return entities;
    });
  }

  onDeleteClick(equipmentId, test = null): void {
    this.toggleEquipmentEnabled(equipmentId).subscribe(
      {
        next: () => {
          this.alertService.success(this.translator.getSentence('delete-equipment'), {
            keepAfterRouteChange: true,
          });
        window.location.reload();
        },
        error: (error) => {
          this.alertService.error(this.translator.getSentence('delete-denied'));
        },
      });
  
  }

  generateEntities(quantity: number, id: string, entitiesLength, component=null){
    //const generateEntitiesForm = this.entityForm.value
    // for (let i = 0; i < generateEntitiesForm.entititiesNumber; i++){
    for (let i = 0; i < quantity; i++){
      let reference = id + "#" + (entitiesLength)
      entitiesLength++

      this.addEquipmentEntity({equipment: id, reference : reference, enabled: true, available: true, state: EntityState[EntityState.EXCELLENT] }).subscribe((
        {
        next: () => {
          return true;
        },
        error: (error) => {
          return error;
        },
        complete: () => {
          if (component) {
            component.router.navigate(['/equipments']).then(() => {
              window.location.reload();
            });
            component.alertService.success(this.translator.getSentence('create-equipment'), {
              keepAfterRouteChange: true,
            });
          }
        }
      }));

    }
    

  }

}
