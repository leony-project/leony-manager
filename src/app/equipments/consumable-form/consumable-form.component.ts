import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../security/auth.service";
import { Select2OptionData } from 'ng-select2';
import { ActivatedRoute, Router } from "@angular/router";
import { Equipment } from 'src/app/models/equipment';
import { ConsumablesService } from 'src/app/consumables/consumables.service';
import { Consumable } from 'src/app/models/consumable';
import { EquipmentsService } from '../equipments.service';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { Subject } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-consumable-form',
  templateUrl: './consumable-form.component.html',
  styleUrls: ['./consumable-form.component.scss']
})
export class ConsumableFormComponent implements OnInit {

  public consumableForm;
  public consumableSelect: Array<Select2OptionData>;
  public equipment: Equipment = new Equipment();
  public consumables: Consumable[] = [];
  public validationForm = true;
  public consumablesTypeahead = new Subject<string>();

  constructor(
    public translator: TranslatorService,
    private consumablesService: ConsumablesService,
    private equipmentsService: EquipmentsService,
    private formBuilder: FormBuilder,
    public auth: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
  ) { }

  ngOnInit(): void {

    this.loadEquipments();
    this.initConsumables();

    const formControls = {
      consumable: new FormControl('', Validators.required),
    };

    if (this.auth.isAdmin()) formControls.consumable = new FormControl('', Validators.required);
    this.consumableForm = this.formBuilder.group(formControls);
  }

  private loadEquipments() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.equipment.id = params.get('id');
      this.equipmentsService.getOne(this.equipment.id).subscribe((data: Equipment) => {
        this.equipment = data;
      });
    });
  }

  private initConsumables(): void {
    this.filterConsumables('');
    this.consumablesTypeahead.subscribe((term) => {
      this.filterConsumables(term);
    });
  }

  private filterConsumables(term): void {
    this.consumablesService.getFiltredConsumable(term).subscribe((consumables) => {
      this.consumables = consumables;
    });
  }


  submit() {
    if (this.consumableForm.dirty && this.consumableForm.valid) {
      this.consumablesService.getOne(this.consumableForm.value.consumable).subscribe((consumable: Consumable) => {
        consumable.equipments.push(this.equipment)
        var equipments = []

        consumable.equipments.forEach(function (e) {
          equipments.push(e.id)
        });

        this.consumablesService.patchConsumable(this.consumableForm.value.consumable, { equipments: equipments }).subscribe(
          {
            next: () => {
              this.alertService.success(this.translator.getSentence('add-equipment'), {
                keepAfterRouteChange: true,
              });
              this.router.navigate(['/equipments', this.equipment.id]).then(() => {
                window.location.reload();
              });
            },
            error: (error) => {
              this.alertService.error(error);
            },
          });

      })
    }
  }

}
