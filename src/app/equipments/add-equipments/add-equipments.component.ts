import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EquipmentsService } from '../equipments.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Equipment } from "../../models/equipment";
import { AlertService } from 'src/app/components/alert/alerts.service';
import bsCustomFileInput from 'bs-custom-file-input';
import { StockPlace } from 'src/app/models/stockPlace';
import { StockPlaceService } from 'src/app/stock-places/stock-place.service';
import { Observable, Subject } from 'rxjs';
import { TagsServices } from 'src/app/tags/tags-services.service';
import { Tag } from 'src/app/models/tag';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-add-equipments',
  templateUrl: './add-equipments.component.html',
  styleUrls: ['./add-equipments.component.scss']
})
export class AddEquipmentsComponent implements OnInit {

  equipmentForm: FormGroup;
  fileUploadForm: FormGroup;
  errors = [];
  stockPlaces: StockPlace[] = [];
  isEditForm = false;
  editFormId: string;
  public stockPlacesTypeahead = new Subject<string>();

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(
    public translator: TranslatorService,
    private fb: FormBuilder,
    private equipmentService: EquipmentsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService,
    private stockPlaceService: StockPlaceService,
    private tagsService: TagsServices,
  ) {
  }

  ngOnInit() {

    // Show image name selected of input
    bsCustomFileInput.init()
    this.initForm();
    this.loadStockPlaces();
    this.loadEquipment();
    this.loadTags();

    this.fileUploadForm = this.fb.group({
      file: ['']
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };


  }
  initForm() {
    this.equipmentForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      description: new FormControl('', [Validators.maxLength(500)]),
      enabled: new FormControl(true, Validators.required),
      image: new FormControl('', [Validators.required]),
      quantity: new FormControl('', [Validators.required]),
      stockPlace: new FormControl('', [Validators.required]),
      tags: new FormControl([], []),
    });
  }

  loadEquipment() {
    // Data recovery for edit
    this.activatedRoute.paramMap.subscribe(params => {
      this.editFormId = params.get('id');
      if (this.editFormId) {
        this.isEditForm = true;
        this.equipmentService.getOne(this.editFormId).subscribe((data: Equipment) => {
          this.equipmentForm = this.fb.group({
            name: new FormControl(data.name, [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
            description: new FormControl(data.description, [Validators.maxLength(500)]),
            image: new FormControl(data.image ? data.image.id : null, [Validators.required]),
            stockPlace: new FormControl(data.stockPlace.id, [Validators.required]),
            tags: [new FormControl(data.tags.map(e => ({ item_id: e.id, item_text: e.name })), [])]
          });
          this.selectedItems = data.tags.map(e => ({ item_id: e.id, item_text: e.name }));
        });
      }
    });
  }

  loadTags() {
    this.tagsService
      .getAllTags()
      .subscribe((data: Tag[]) => {
        this.dropdownList = data.map(e => ({ item_id: e.id, item_text: e.name }));
      });

  }

  handleFileInput(files: FileList): void {
    this.fileUploadForm.get('file').setValue(files.item(0));
    this.equipmentService.postFile(this.fileUploadForm).subscribe(data => {
      this.equipmentForm.get('image').setValue(data.id);
    });
  }

  submit(): any {
    this.equipmentForm.get('tags').setValue(this.selectedItems.map(e => e.item_id));
    console.log(this.equipmentForm);

    if (!this.isEditForm) {
      this.create();
    } else {
      this.update();
    }
  }

  create(): any {
    const equipment = this.equipmentForm.value;
    if (this.equipmentForm.valid) {
      this.equipmentService.addEquipment(equipment).subscribe({
        next: (data) => { 
          this.equipmentService.generateEntities(this.equipmentForm.value.quantity, data.id, 0, this);
          
        }, 
        error: (error) => {
            this.alertService.error(error);
          },
        });
    }
  }

  update(): any {
    const equipment = this.equipmentForm.value;
    if (this.equipmentForm.valid) {
      this.equipmentService.patchEquipment(this.editFormId, equipment).subscribe(
        {
          next: () => {
            this.router.navigate(['/equipments', this.editFormId]).then(() => {
              window.location.reload();
            });
            this.alertService.success(this.translator.getSentence('equipment-image'), {
              keepAfterRouteChange: true,
            });
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  private loadStockPlaces(): void {
    this.filterStockPlaces('');
    this.stockPlacesTypeahead.subscribe((term) => {
      this.filterStockPlaces(term);
    });
  }

  private filterStockPlaces(term): void {
    this.stockPlaceService.getFiltredStockPlace(term).subscribe((data) => {
      this.stockPlaces = data;
    });
  }

}
