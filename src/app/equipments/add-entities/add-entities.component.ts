import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EquipmentsService } from '../equipments.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { Entity } from 'src/app/models/entity';
import { EntityState } from 'src/app/models/EntityState.enum';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-add-entities',
  templateUrl: './add-entities.component.html',
  styleUrls: ['./add-entities.component.scss']
})
export class AddEntitiesComponent implements OnInit {

  equipmentInstanceForm: FormGroup;
  equipmentId: number;
  entity: Entity = new Entity();
  errors = [];
  states = EntityState;
  keys: string[];


  constructor(
    public translator: TranslatorService,
    private form: FormBuilder,
    private equipmentService: EquipmentsService,
    public modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
  ) {
  }

  ngOnInit(): void {

    this.equipmentId = +this.activatedRoute.snapshot.paramMap.get('id');

    this.initForm();
    this.reloadData(this.equipmentId);

    this.keys = Object.keys(this.states).filter(k => !isNaN(Number(k)));

  }


  initForm() {
    this.equipmentInstanceForm = this.form.group({
      enabled: new FormControl(''),
      available: new FormControl(''),
      reference: new FormControl(''),
      state: new FormControl(''),
    });
  }

  reloadData(equipmentId) {
    this.equipmentService
      .getOneEntity(equipmentId)
      .subscribe((data: Entity) => {
        this.entity = data;
        this.equipmentInstanceForm = this.form.group({
          enabled: new FormControl(data.enabled, Validators.required),
          available: new FormControl(data.available, Validators.required),
          reference: new FormControl(data.reference, Validators.required),
          state: new FormControl(data.state, Validators.required)
        });
      });

  }


  update(): any {
    const entity = this.equipmentInstanceForm.value;
    entity.state = EntityState[entity.state]
    if (this.equipmentInstanceForm.valid) {
      this.equipmentService.patchEntity(this.entity.id, entity).subscribe(
        {
          next: () => {
            this.router.navigate(['/equipments', this.entity.equipment.id]).then(() => {
              window.location.reload();
            });
            this.alertService.success(this.translator.getSentence('edit-entity'), {
              keepAfterRouteChange: true,
            });
          },
          error: (error) => {
            this.alertService.error(error);
          },
        });
    }
  }



}


