import { SearchComponent } from './search/search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EquipmentsListComponent } from './equipments/equipments-list/equipments-list.component';
import { EquipmentDetailsComponent } from './equipments/equipment-details/equipment-details.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AuthenticatedGuard } from './security/authenticated.guard';
import { AdminGuard } from './security/admin.guard';
import { UsersComponent } from './users/list/users.component';
import { LendingsListComponent } from './lendings/lendings-list/lendings-list.component';
import { LendingFormComponent } from './lendings/lending-form/lending-form.component';
import { AddEntitiesComponent } from './equipments/add-entities/add-entities.component';
import { ConsumableFormComponent } from './equipments/consumable-form/consumable-form.component';
import { AddUserComponent } from "./users/add-user/add-user.component";
import { LendingReturnFormComponent } from "./lendings/lending-return-form/lending-return-form.component";
import { LendingsEntityHistoryComponent } from "./lendings/lendings-entity-history/lendings-entity-history.component";
import { ConsumablesListComponent } from "./consumables/consumables-list/consumables-list.component";
import { ConsumablesDetailsComponent } from "./consumables/consumables-details/consumables-details.component";
import { AddConsumablesComponent } from "./consumables/add-consumables/add-consumables.component";
import { LendingDetailsComponent } from './lendings/lending-details/lending-details.component';
import { InOutConsumablesComponent } from "./in-out-consumables/in-out-consumables/in-out-consumables.component";
import { LoggingComponent } from "./logging/logs/log.component";
import { TagsManagerComponent } from './tags/tags-manager/tags-manager.component';
import { AddEquipmentsComponent } from './equipments/add-equipments/add-equipments.component';
import { StockPlaceListComponent } from "./stock-places/stock-place-list/stock-place-list.component";
import { StockPlaceFormComponent } from "./stock-places/stock-place-form/stock-place-form.component";
import { StockPlaceDetailsComponent } from "./stock-places/stock-place-details/stock-place-details.component";
import { UserSeeComponent } from './users/user-see/user-see.component';
import { EntranceFormComponent } from './in-out-consumables/entrance-form/entrance-form.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UsersComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'users/add', component: AddUserComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'users/:id/edit', component: AddUserComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'users/:id', component: UserSeeComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'lendings', component: LendingsListComponent, canActivate: [AuthenticatedGuard] },
  { path: 'logging', component: LoggingComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'in-out-consumables', component: InOutConsumablesComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'in-out-consumables/add', component: EntranceFormComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'tags-manager', component: TagsManagerComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'lendings/add', component: LendingFormComponent, canActivate: [AuthenticatedGuard] },
  { path: 'lendings/add/equipment/:id', component: LendingFormComponent, canActivate: [AuthenticatedGuard] },
  { path: 'lendings/add/consumable/:id', component: LendingFormComponent, canActivate: [AuthenticatedGuard] },
  { path: 'lendings/:id', component: LendingDetailsComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'lendings/return/:id', component: LendingReturnFormComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'lendings/entity/:id', component: LendingsEntityHistoryComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'equipments', component: EquipmentsListComponent },
  { path: 'equipments/add', component: AddEquipmentsComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'equipments/:id', component: EquipmentDetailsComponent },
  { path: 'equipments/:id/edit', component: AddEquipmentsComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'equipments/:id/add', component: AddEntitiesComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'equipments/:id/entity/:id', component: AddEntitiesComponent },
  { path: 'equipments/:id/addConsumable', component: ConsumableFormComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'consumables', component: ConsumablesListComponent },
  { path: 'consumables/add', component: AddConsumablesComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'consumables/:id', component: ConsumablesDetailsComponent },
  { path: 'consumables/:id/edit', component: AddConsumablesComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'stock-place', component: StockPlaceListComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'stock-place/add', component: StockPlaceFormComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'stock-place/:id', component: StockPlaceDetailsComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'stock-place/:id/edit', component: StockPlaceFormComponent, canActivate: [AuthenticatedGuard, AdminGuard] },
  { path: 'search/:searchCriteria', component: SearchComponent },
  { path: '**', pathMatch: 'full', component: PagenotfoundComponent },

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
