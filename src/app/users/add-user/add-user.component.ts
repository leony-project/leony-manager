import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { UsersService } from "../users.service";
import { ActivatedRoute, Router } from "@angular/router";
import { User } from "../../models/user";
import { AlertService } from 'src/app/components/alert/alerts.service';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss', '../../../form.scss']
})
export class AddUserComponent implements OnInit {

  public userForm: FormGroup;
  public user: User;
  public update: boolean;

  constructor(
    public translator: TranslatorService,
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
  ) { }

  ngOnInit(): void {
    let formControls = {
      'email': new FormControl('', Validators.required),
      'firstName': new FormControl('', Validators.required),
      'lastName': new FormControl('', Validators.required),
      'password': new FormControl(''),
      'phone': new FormControl(''),
      'git': new FormControl(''),
      'isAdmin': new FormControl(false, Validators.required)
    };

    this.activatedRoute.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.update = true;
        this.usersService.getOneById(params.get('id')).subscribe((data: User) => {
          this.user = data;
          formControls.email.setValue(this.user.email);
          formControls.firstName.setValue(this.user.firstName);
          formControls.lastName.setValue(this.user.lastName);
          formControls.phone.setValue(this.user.phone);
          formControls.git.setValue(this.user.git);
          formControls.isAdmin.setValue(this.user.isAdmin);
        });
      }
    });

    this.userForm = this.formBuilder.group(formControls);
  }

  submit() {
    if (this.userForm.dirty && this.userForm.valid) {
      this.usersService.insert(this.userForm.value).subscribe(() => {
        $('.loader').hide();
        this.router.navigate(['users']).then(() => {
          window.location.reload();
        });
      });
    }
  }

  save() {
    if (this.userForm.dirty && this.userForm.valid) {
      this.usersService.patch(this.user.id, this.userForm.value).subscribe(() => {
        this.router.navigate(['users']).then(() => {
          this.alertService.success(this.translator.getSentence('update-coord'))
          window.location.reload();
        });
      });
    }
    else {
      this.alertService.error(this.translator.getSentence('sending-error'));
    }
  }
}
