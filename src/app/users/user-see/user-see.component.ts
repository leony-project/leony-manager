
import { Component, OnInit } from '@angular/core';

import { UsersService } from "../users.service";
import { ActivatedRoute } from "@angular/router";
import { User } from "../../models/user";

import { AuthService } from 'src/app/security/auth.service';
import { Observable } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';



@Component({
  selector: 'app-user-see',
  templateUrl: './user-see.component.html',
  styleUrls: ['./user-see.component.scss']
})
export class UserSeeComponent implements OnInit {
  user: Observable<User>;

  constructor(
    public translator: TranslatorService,
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute,
    public auth: AuthService,

  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.user = this.usersService.getOneById(params.get('id'));
      }
    });
  };
}
