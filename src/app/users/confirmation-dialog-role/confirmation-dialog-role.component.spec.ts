import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationDialogRoleComponent } from './confirmation-dialog-role.component';

describe('ConfirmationDialogRoleComponent', () => {
  let component: ConfirmationDialogRoleComponent;
  let fixture: ComponentFixture<ConfirmationDialogRoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmationDialogRoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationDialogRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
