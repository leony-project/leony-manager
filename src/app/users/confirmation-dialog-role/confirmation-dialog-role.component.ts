import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/models/user';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-confirmation-dialog-role',
  templateUrl: './confirmation-dialog-role.component.html',
  styleUrls: ['./confirmation-dialog-role.component.scss'],
})
export class ConfirmationDialogRoleComponent implements OnInit {
  @Input() fromParent;
  user: User;
  constructor(public translator: TranslatorService, public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.user = this.fromParent.user;
  }
}
