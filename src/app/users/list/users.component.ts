import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { User } from '../../models/user';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogRoleComponent } from '../confirmation-dialog-role/confirmation-dialog-role.component';
import { Router } from '@angular/router';
import { TranslatorService } from 'src/app/translator/translator.service';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  private itemsNumberPerPage = 10;
  public usersPage: number = 1;
  public usersPages: Array<number> = new Array<0>();
  users: User[];
  userLoaded: Promise<boolean>;
  modalReference: any;
  closeResult = '';
  currentUser: any;

  constructor(
    public translator: TranslatorService,
    private userService: UsersService,
    private alertService: AlertService,
    private modalService: NgbModal,
    private route: Router
  ) { }

  ngOnInit() {
    this.loadUsers();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  loadUsers() {
    this.userService.getAll(this.usersPage).subscribe((users: User[]) => {
      this.usersPages = Array.from(Array(Math.ceil(users['totalItems'] / this.itemsNumberPerPage)).keys());
      this.users = users;
      this.userLoaded = Promise.resolve(true);
      ({ error }) => this.alertService.error(error.title);
    });
  }

  delete(user: User) {
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('user-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.userService.toggleUserEnabled(user).subscribe(
        {
          next: () => {
            this.loadUsers();
            this.alertService.success(this.translator.getSentence('user-deleted'), {
              keepAfterRouteChange: true,
            });
          },
          error: (error) => {
            this.alertService.error(error);
          }
        });
    }
  }

  openConfirmationDialog(user: User, isAdminPromotion: boolean) {
    this.modalReference = this.modalService.open(
      ConfirmationDialogRoleComponent,
      {
        ariaLabelledBy: 'modal-basic-title',
      }
    );
    this.modalReference.componentInstance.fromParent = {
      user: user,
    };
    this.modalReference.result.then(
      (result) => {
        if (result) {
          user.isAdmin = isAdminPromotion;
          this.userService.patch(user.id, user).subscribe(
            () => {
              this.alertService.success(this.translator.getSentence('user-promote'));
              this.loadUsers();
            },
            ({ error }) => this.alertService.error(error.title)
          );
        }
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updatePage(n) {

    this.usersPage = n;
    this.loadUsers();

  }
}
