import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { User } from "../models/user";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiURL = environment.rest_endpoint;
  patchHeaders = {
    'Content-Type': 'application/merge-patch+json',
    Accept : 'application/json'
  };
  constructor(private http: HttpClient) { }

  getOneById(id: string): Observable<User> {
    return this.http.get<User>(`${this.apiURL}users/${id}`);
  }

  getAll(page = 1): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiURL}users?page=${page}`);
  }

  search(term: string): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiURL}users?lastName=${term}`);
  }

  delete(user: User) {
    return this.http.delete(`${this.apiURL}users/${user.id}`);
  }

  insert(user: User):Observable<User> {
    user.enabled = true;
    return this.http.post<User>(`${this.apiURL}users`, user);
  }

  patch(id, user: User):Observable<User> {
    return this.http.put<User>(`${this.apiURL}users/${id}`, user);
  }
  toggleUserEnabled(user: User): Observable<User> {
    
    return this.http.post<User>(`${this.apiURL}users/${user.id}/toggle_enabled`,{});
  }
}
