import { Component, OnInit } from '@angular/core';
import { TranslatorService } from '../translator/translator.service';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.scss']
})
export class PagenotfoundComponent implements OnInit {

  constructor(public translator: TranslatorService) { }

  ngOnInit() {
    $('#wrap-content').css('padding-top', 0).css('padding-left', 0);
  }

}
