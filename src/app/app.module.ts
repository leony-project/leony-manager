import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EquipmentsListComponent } from './equipments/equipments-list/equipments-list.component';
import { AddEquipmentsComponent } from './equipments/add-equipments/add-equipments.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { EquipmentDetailsComponent } from './equipments/equipment-details/equipment-details.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomepageComponent } from './homepage/homepage.component';
import { UsersComponent } from './users/list/users.component';
import { AcceptHeadersInterceptor } from './security/accept-headers.interceptor';
import { TokenInterceptor } from './security/token.interceptor';
import { LendingsListComponent } from './lendings/lendings-list/lendings-list.component';
import { LendingFormComponent } from './lendings/lending-form/lending-form.component';
import { LendingDetailsComponent } from './lendings/lending-details/lending-details.component';
import { AddEntitiesComponent } from './equipments/add-entities/add-entities.component';
import { NgSelect2Module } from 'ng-select2';
import { AlertModule } from './components/alert/alert.module';
import { ConsumableFormComponent } from './equipments/consumable-form/consumable-form.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LendingReturnFormComponent } from './lendings/lending-return-form/lending-return-form.component';
import { LendingsEntityHistoryComponent } from './lendings/lendings-entity-history/lendings-entity-history.component';
import { ConsumablesListComponent } from './consumables/consumables-list/consumables-list.component';
import { ConsumablesDetailsComponent } from './consumables/consumables-details/consumables-details.component';
import { AddConsumablesComponent } from './consumables/add-consumables/add-consumables.component';
import { InOutConsumablesComponent } from './in-out-consumables/in-out-consumables/in-out-consumables.component';
import { TagsManagerComponent } from './tags/tags-manager/tags-manager.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ListTableLendingsComponent } from './lendings/list-table-lendings/list-table-lendings.component';
import { QuillModule } from 'ngx-quill';
import { LoggingComponent } from './logging/logs/log.component';
import { TagInputComponent } from './tags/tag-input/tag-input.component';
import { StockPlaceDetailsComponent } from './stock-places/stock-place-details/stock-place-details.component';
import { StockPlaceFormComponent } from './stock-places/stock-place-form/stock-place-form.component';
import { StockPlaceListComponent } from './stock-places/stock-place-list/stock-place-list.component';
import { ConfirmationDialogRoleComponent } from './users/confirmation-dialog-role/confirmation-dialog-role.component';
import { SearchComponent } from './search/search.component';
import { HydraInterceptor } from "./security/hydra.interceptor";
import { NgSelectModule } from '@ng-select/ng-select';
import { UserSeeComponent } from './users/user-see/user-see.component';
import { UserLendingsComponent } from './lendings/lendings-list/user-lendings/user-lendings.component';
import { AuthInterceptor } from './security/AuthInterceptor ';
import { EntranceFormComponent } from './in-out-consumables/entrance-form/entrance-form.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { LendingCardComponent } from './lendings/lending-card/lending-card.component';
import { LeftNavbarComponent } from './navbar/left-navbar/left-navbar.component';
import { TranslatorService } from './translator/translator.service';
import { TranslatorButtonComponent } from './translator/translator-button/translator-button.component';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    AddEquipmentsComponent,
    EquipmentsListComponent,
    EquipmentDetailsComponent,
    HomepageComponent,
    UsersComponent,
    LendingsListComponent,
    LendingFormComponent,
    AddEntitiesComponent,
    ConsumableFormComponent,
    AddUserComponent,
    LendingDetailsComponent,
    LendingReturnFormComponent,
    LendingsEntityHistoryComponent,
    ConsumablesListComponent,
    ConsumablesDetailsComponent,
    AddConsumablesComponent,
    InOutConsumablesComponent,
    TagsManagerComponent,
    ListTableLendingsComponent,
    TagInputComponent,
    LoggingComponent,
    StockPlaceDetailsComponent,
    StockPlaceFormComponent,
    StockPlaceListComponent,
    ConfirmationDialogRoleComponent,
    SearchComponent,
    UserSeeComponent,
    UserLendingsComponent,
    EntranceFormComponent,
    PagenotfoundComponent,
    LendingCardComponent,
    LeftNavbarComponent,
    TranslatorButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    AlertModule,
    HttpClientModule,
    NgSelect2Module,
    NgSelectModule,
    OwlDateTimeModule,
    BrowserAnimationsModule,
    OwlNativeDateTimeModule,
    FontAwesomeModule,
    NgMultiSelectDropDownModule.forRoot(),
    QuillModule.forRoot(),
    NgSelect2Module,
  ],

  providers: [
    TranslatorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HydraInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: OWL_DATE_TIME_LOCALE,
      useValue: 'fr',
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
