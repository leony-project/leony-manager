import { Consumable } from 'src/app/models/consumable';
export class Entrance {
  id?: string;
  entrance: boolean;
  consumable:any;
  quantity: number;
  date: Date;
  enabled: boolean;
}
