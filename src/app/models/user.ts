import { Lending } from './lending';
import { Role } from './role';

export class User {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  git: string;
  phone: string;
  lendings: Lending[] = [];
  isAdmin: boolean = false;
  role: Role;
  enabled: boolean;
}
