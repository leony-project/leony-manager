export class CheckLending{
    status: boolean;
    data: Array<CheckData>;
}

export class CheckData{
    id: number;
    status: boolean;
    type: string;
    quantity: number;
    returnMax?: number;
    lendingMax?: number;
    quantityMax?: number;
    entities?: Array<number>;
}