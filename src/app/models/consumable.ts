import { Equipment } from "./equipment";
import { Tag } from "./tag";
import { MediaObject } from "./mediaObject";
import { StockPlace } from "./stockPlace";

export class Consumable {
    id: string;
    description: string;
    name: string;
    image: MediaObject;
    quantity: bigint;
    unity: string;
    enabled: boolean;
    tags: Tag[];
    equipments: Equipment[];
    stockPlace: StockPlace;
}
