import {Entity} from './entity';
import {User} from './user';
import {Consumable} from "./consumable";

export class Lending {
  id: string;
  entity: Entity;
  consumable: Consumable;
  lendingQuantity: bigint;
  returnQuantity: bigint;
  userLending: User;
  userReturn: User;
  commentLending: string;
  commentReturn: string;
  lendingDatetime: Date;
  returnDatetime: Date;
  accepted: boolean;
  returned: boolean;
  enabled: boolean;
}
