import { EntityState } from './EntityState.enum';
import {Equipment} from './equipment';

export class Entity {
  id: string;
  equipment: Equipment;
  enabled: boolean;
  reference: string;
  available: boolean;
  state: EntityState;
}
