import {Equipment} from './equipment';
import {Consumable} from './consumable';

export class StockPlace {
  id: string;
  name: string;
  consumables: Consumable[];
  equipments: Equipment [];
  enabled: boolean;
}
