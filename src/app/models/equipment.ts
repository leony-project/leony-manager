import {Entity} from './entity';
import {Lending} from "./lending";
import {Tag} from "./tag";
import {Consumable} from "./consumable";
import {MediaObject} from "./mediaObject";
import {StockPlace} from "./stockPlace";


export class Equipment {
  id: string;
  name: string;
  image: MediaObject;
  enabled: boolean;
  description: string;
  entities: Entity[];
  lendings: Lending[];
  tags: Tag[];
  consumables: Consumable[];
  entitiesAvailable: number;
  stockPlace: StockPlace;
}
