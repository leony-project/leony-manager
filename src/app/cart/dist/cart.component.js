"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CartComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var environment_1 = require("src/environments/environment");
var CartComponent = /** @class */ (function () {
    function CartComponent(formBuilder, auth, lendingService, cartService, equipmentsService, consumableService, router, alertService) {
        this.formBuilder = formBuilder;
        this.auth = auth;
        this.lendingService = lendingService;
        this.cartService = cartService;
        this.equipmentsService = equipmentsService;
        this.consumableService = consumableService;
        this.router = router;
        this.alertService = alertService;
        this.restBaseUri = environment_1.environment.base_endpoint;
        this.todayDate = new Date();
        this.user = null;
    }
    //form array de form control et boucler dessus
    CartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadCart();
        if (!this.auth.isAdmin()) {
            this.auth.getCurrentUser().subscribe(function (user_response) {
                _this.user = user_response.id;
            });
        }
        this.entities = [];
        this.formControls = {
            user: new forms_1.FormControl(this.auth.currentUserValue.id, forms_1.Validators.required),
            entity: new forms_1.FormControl(null),
            consumable: new forms_1.FormControl(null),
            lendingQuantity: new forms_1.FormControl(1, forms_1.Validators.min(1)),
            lendingDatetime: new forms_1.FormControl(null),
            returnDatetime: new forms_1.FormControl(null),
            commentLending: new forms_1.FormControl(null)
        };
        this.lendingForm = this.formBuilder.group(this.formControls);
    };
    CartComponent.prototype.loadCart = function () {
        var _this = this;
        this.cart = (JSON.parse(sessionStorage.getItem('cart')) == null ? [] : JSON.parse(sessionStorage.getItem('cart')));
        // ! Voir l'initialisation de la variable
        this.equipmentsInCart = Array();
        this.cart.forEach(function (e, i) {
            if (e.type == 'CONSUMABLE') {
                _this.consumableService.getOne(e.id).subscribe(function (data) {
                    data.type = 'CONSUMABLE';
                    _this.equipmentsInCart.push(data);
                });
            }
            else if (e.type == 'EQUIPMENT') {
                _this.equipmentsService.getOne(e.id).subscribe(function (data) {
                    data.type = 'EQUIPMENT';
                    _this.equipmentsInCart.push(data);
                });
            }
        });
    };
    CartComponent.prototype.submit = function () {
        var _this = this;
        var params = [];
        this.cart.forEach(function (e, i) {
            var getParams = {
                id: e.id,
                lendingDatetime: _this.lendingForm.value.lendingDatetime.getTime(),
                returnDatetime: _this.lendingForm.value.returnDatetime.getTime(),
                quantity: e.quantity,
                type: e.type
            };
            params.push(getParams);
        });
        this.cartService.checkLendings(params).subscribe(function (response) {
            var status = response.status;
            response.data.forEach(function (e, i) {
                if (status) {
                    if (_this.user == null)
                        _this.user = _this.lendingForm.value.user;
                    if (e.type == 'EQUIPMENT') {
                        for (var j = 0; j < e.quantity; j++) {
                            var submitForm = _this.createSubmitForm(_this.lendingForm.value, _this.user);
                            submitForm.entity = e.entities[0];
                            e.entities.splice(0, 1);
                            _this.requestLending(submitForm);
                        }
                    }
                    else if (e.type == 'CONSUMABLE') {
                        var submitForm = _this.createSubmitForm(_this.lendingForm.value, _this.user);
                        submitForm.consumable = e.id;
                        submitForm.lendingQuantity = e.quantity;
                        _this.consumableService.patchConsumable(e.id, { quantity: e.quantityMax - e.quantity }).subscribe(function (data) { console.log(data); });
                        _this.requestLending(submitForm);
                    }
                    sessionStorage.removeItem('cart');
                }
                else {
                    if (e.status) {
                        return;
                    }
                    else {
                        if (e.type == 'EQUIPMENT') {
                            if (e.returnMax != undefined) {
                                var sentence = _this.getSentenceReturnMax(e.returnMax);
                                console.log(sentence);
                            }
                            if (e.lendingMax != undefined) {
                                var sentence = _this.getSentenceLendingMax(e.lendingMax);
                                console.log(sentence);
                            }
                        }
                        else if (e.type == 'CONSUMABLE') {
                            console.log(e.quantityMax);
                        }
                    }
                }
            });
        });
    };
    CartComponent.prototype.deleteFromCart = function (item, i) {
        var result = this.cart.filter(function (e) {
            return [item.id].includes(e.equipment_id) && e.type == item.type;
        });
        var index = this.cart.indexOf(result[0]);
        this.equipmentsInCart.splice(i, 1);
        this.cart.splice(index, 1);
        sessionStorage.setItem('cart', JSON.stringify(this.cart));
    };
    CartComponent.prototype.getDateCart = function (dateMax) {
        var date = new Date(dateMax);
        var day = date.getUTCDate();
        var month = date.getUTCMonth() + 1;
        var year = date.getFullYear();
        return {
            day: day,
            month: month,
            year: year
        };
    };
    CartComponent.prototype.getSentenceReturnMax = function (returnMax) {
        var date = this.getDateCart(returnMax);
        return "Available till " + date.day + '/' + date.month + '/' + date.year;
    };
    CartComponent.prototype.getSentenceLendingMax = function (lendingMax) {
        var date = this.getDateCart(lendingMax);
        return "Available from " + date.day + '/' + date.month + '/' + date.year;
    };
    CartComponent.prototype.createSubmitForm = function (value, user) {
        var submitForm = {
            user: user,
            entity: null,
            consumable: null,
            lendingQuantity: null,
            lendingDatetime: value.lendingDatetime,
            returnDatetime: value.returnDatetime,
            commentLending: value.commentLending,
            accepted: (this.auth.isAdmin()) ? true : false,
            returned: false
        };
        return submitForm;
    };
    CartComponent.prototype.requestLending = function (submitForm) {
        var _this = this;
        this.lendingService.insert(submitForm).subscribe(function (data) {
            console.log(data);
        }, function (error) {
            _this.alertService.error(error.error['hydra:description']);
        });
    };
    CartComponent = __decorate([
        core_1.Component({
            selector: 'app-cart',
            templateUrl: './cart.component.html',
            styleUrls: ['./cart.component.scss']
        })
    ], CartComponent);
    return CartComponent;
}());
exports.CartComponent = CartComponent;
