import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CheckLending } from '../models/checkLending';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  restBaseUri = environment.rest_endpoint;
  constructor(private http: HttpClient) {
  }

  checkLendings(data){
    return this.http.post(this.restBaseUri + `checklending`, data);
  }
}
