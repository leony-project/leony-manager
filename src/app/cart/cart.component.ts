import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../security/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { collapseTextChangeRangesAcrossMultipleVersions, HighlightSpanKind } from 'typescript';
import { EquipmentsService } from '../equipments/equipments.service';
import { LendingsService } from '../lendings/lendings.service';
import { CartService } from './cart.service';
import { Router } from '@angular/router';
import { AlertService } from '../components/alert/alerts.service';
import { Equipment } from '../models/equipment';
import { environment } from 'src/environments/environment';
import { ConsumablesService } from '../consumables/consumables.service';
import { Consumable } from '../models/consumable';
import { CheckLending } from '../models/checkLending';
import { param } from 'jquery';
import { faMinusCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { TranslatorService } from '../translator/translator.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss',
    '../../form.scss']
})
export class CartComponent implements OnInit {
  restBaseUri = environment.base_endpoint;
  public cart: any[];
  public finalCart: any[];
  public equipmentsInCart: any[];
  private formControls;
  public lendingForm;
  public todayDate: any = new Date();
  private entities: any[];
  private consumables: any[];
  private user = null;
  faMinusCircle = faMinusCircle;
  faPlusCircle = faPlusCircle;

  constructor(
    private formBuilder: FormBuilder,
    public translator: TranslatorService,
    public auth: AuthService,
    private lendingService: LendingsService,
    private cartService: CartService,
    private equipmentsService: EquipmentsService,
    private consumableService: ConsumablesService,
    private router: Router,
    private alertService: AlertService
  ) { }
  //form array de form control et boucler dessus
  ngOnInit(): void {
    this.loadCart();
    if (!this.auth.isAdmin()) {
      this.auth.getCurrentUser().subscribe((user_response) => {
        this.user = user_response.id;
      });
    }

    this.entities = [];
    this.formControls = {
      user: new FormControl(this.auth.currentUserValue.id, Validators.required),
      entity: new FormControl(null),
      consumable: new FormControl(null),
      lendingQuantity: new FormControl(1, Validators.min(1)),
      lendingDatetime: new FormControl(null),
      returnDatetime: new FormControl(null),
      commentLending: new FormControl(null)
    };
    this.lendingForm = this.formBuilder.group(this.formControls);
  }

  loadCart(): any {
    this.cart = (JSON.parse(sessionStorage.getItem('cart')) == null ? [] : JSON.parse(sessionStorage.getItem('cart')));
    // ! Voir l'initialisation de la variable
    this.equipmentsInCart = Array<any>();
    this.cart.forEach((e, i) => {
      if (e.type == 'CONSUMABLE') {
        this.consumableService.getOne(e.id).subscribe((data: any) => {
          data.type = 'CONSUMABLE';
          this.equipmentsInCart.push(data);
        });
      } else if (e.type == 'EQUIPMENT') {
        this.equipmentsService.getOne(e.id).subscribe((data: any) => {
          data.type = 'EQUIPMENT';
          this.equipmentsInCart.push(data);
        });
      }
    });
  }


  submit(): void {
    let params = []
    this.cart.forEach((e, i) => {
      let getParams = {
        id: e.id,
        lendingDatetime: this.lendingForm.value.lendingDatetime.getTime(),
        returnDatetime: this.lendingForm.value.returnDatetime.getTime(),
        quantity: e.quantity,
        type: e.type
      }
      params.push(getParams);
    });
    this.cartService.checkLendings(params).subscribe((response: CheckLending) => {
      let status = response.status;
      response.data.forEach((e, i) => {
        if (status) {
          if (this.user == null)
            this.user = this.lendingForm.value.user;

          if (e.type == 'EQUIPMENT') {
            for (let j = 0; j < e.quantity; j++) {
              let submitForm = this.createSubmitForm(this.lendingForm.value, this.user);
              submitForm.entity = e.entities[0];
              e.entities.splice(0, 1);
              this.requestLending(submitForm);
            }
          } else if (e.type == 'CONSUMABLE') {
            let submitForm = this.createSubmitForm(this.lendingForm.value, this.user);
            submitForm.consumable = e.id;
            submitForm.lendingQuantity = e.quantity;
            this.consumableService.patchConsumable(e.id, { quantity: e.quantityMax - e.quantity }).subscribe((data) => { console.log(data) });
            this.requestLending(submitForm);
          }
          sessionStorage.removeItem('cart');
        } else {
          if (e.status) {
            return;
          } else {
            if (e.type == 'EQUIPMENT') {
              if (e.returnMax != undefined) {
                let sentence = this.getSentenceReturnMax(e.returnMax);
                console.log(sentence);
              }
              if (e.lendingMax != undefined) {
                let sentence = this.getSentenceLendingMax(e.lendingMax);
                console.log(sentence);
              }
            } else if (e.type == 'CONSUMABLE') {
              console.log(e.quantityMax);
            }
          }
        }
      });
    });
  }

  deleteFromCart(item, i): void {
    var result = this.cart.filter(function (e) {
      return [item.id].includes(e.id) && e.type == item.type;
    });
    let index = this.cart.indexOf(result[0]);
    this.equipmentsInCart.splice(i, 1);
    this.cart.splice(index, 1);
    sessionStorage.setItem('cart', JSON.stringify(this.cart));
  }

  getDateCart(dateMax: Date) {
    let date = new Date(dateMax);
    let day = date.getUTCDate();
    let month = date.getUTCMonth() + 1;
    let year = date.getFullYear();
    return {
      day: day,
      month: month,
      year: year
    }
  }

  getSentenceReturnMax(returnMax) {
    let date = this.getDateCart(returnMax);
    return "Available till " + date.day + '/' + date.month + '/' + date.year;
  }

  getSentenceLendingMax(lendingMax) {
    let date = this.getDateCart(lendingMax);
    return "Available from " + date.day + '/' + date.month + '/' + date.year;
  }

  createSubmitForm(value, user) {
    let submitForm = {
      user: user,
      entity: null,
      consumable: null,
      lendingQuantity: null,
      lendingDatetime: value.lendingDatetime,
      returnDatetime: value.returnDatetime,
      commentLending: value.commentLending,
      accepted: (this.auth.isAdmin()) ? true : false,
      returned: false
    };
    return submitForm;
  }

  requestLending(submitForm) {
    this.lendingService.insert(submitForm).subscribe((data) => {
      console.log(data);
    }, error => {
      this.alertService.error(error.error['hydra:description']);
    });
  }

  updateQuantity(i, quantity) {
    if ((this.cart[i].quantity + quantity) == 0)
      return;
    this.cart[i].quantity += quantity;
    sessionStorage.setItem('cart', JSON.stringify(this.cart));
  }

}
