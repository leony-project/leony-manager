import { Entrance } from './../models/entrance';
import { Consumable } from './../models/consumable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InOutConsumablesService {
  restBaseUri = environment.rest_endpoint;

  patchHeaders = {
    'Content-Type': 'application/merge-patch+json',
    Accept : 'application/json'
  };

  constructor(private http: HttpClient) { }

  getAll(page = 1): Observable<Consumable[]>{
    const url = this.restBaseUri + 'consumables?page=' + page;
    return this.http.get<Consumable[]>(url);
  }
  getAllInOutConsumables(page = 1): Observable<any>{
    const url = this.restBaseUri + 'consumable_entrances?page=' + page;
    return this.http.get<Entrance[]>(url);
  }

  deleteInOutConsumable(entrance: Entrance){
    return this.http.delete(this.restBaseUri + 'consumable_entrances/'+ entrance.id);
  }
  
  toggleInOutConsumableEnabled(id): Observable<Entrance> {
    return this.http.post<Entrance>(`${this.restBaseUri}consumable_entrances/${id}/toggle_enabled`, {});
  }
  
  addInOutConsumable(params:Entrance) {
    const url = this.restBaseUri + 'consumable_entrances';
    return this.http.post<Entrance>(url, params);
  }


}
