import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { ConsumablesService } from 'src/app/consumables/consumables.service';
import { Consumable } from 'src/app/models/consumable';
import { Entrance } from 'src/app/models/entrance';
import { AuthService } from 'src/app/security/auth.service';
import { TranslatorService } from 'src/app/translator/translator.service';
import { InOutConsumablesService } from '../in-out-consumables.service';

@Component({
  selector: 'app-entrance-form',
  templateUrl: './entrance-form.component.html',
  styleUrls: ['./entrance-form.component.scss', '../../../form.scss']
})
export class EntranceFormComponent implements OnInit {

  public inOutForm: FormGroup;
  public newEntrance: Entrance;
  public consumablesTypeahead = new Subject<string>();
  public errors = [];
  public consumables: Consumable[] = [];

  constructor(
    public translator: TranslatorService,
    private inOutConsumablesServices: InOutConsumablesService,
    public auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private consumablesService: ConsumablesService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {

    this.initForm();
    this.initConsumables();
  }

  initForm() {
    const formControls = {
      entrance: new FormControl(true, Validators.required),
      date: new FormControl(new Date(), Validators.required),
      consumable: new FormControl('Consommables', Validators.required),
      quantity: new FormControl(1, Validators.required)
    };
    this.inOutForm = this.formBuilder.group(formControls);
  }

  private initConsumables(): void {
    this.filterConsumables('');
    this.consumablesTypeahead.subscribe((term) => {
      this.filterConsumables(term);
    });
  }

  private filterConsumables(term): void {
    this.consumablesService.getFiltredConsumable(term).subscribe((consumables) => {
      this.consumables = consumables;
    });
  }



  add() {
    if (this.inOutForm.valid && this.inOutForm.controls['consumable'].value != "Consommables") {
      this.newEntrance = this.inOutForm.value;
      this.inOutConsumablesServices.addInOutConsumable(this.newEntrance).subscribe((data) => {
        if (data.id) {
          this.router.navigate(['in-out-consumables']).then(() => {
            window.location.reload();
          });

        } else {
          this.errors.push('Invalid data received');
        }
      }, (error) => {
        this.errors.push(error.message);
        console.error(error);
      });
    }
    else {
      this.alertService.error(this.translator.getSentence('error-consumable-not-selected'))
    }

  }


}
