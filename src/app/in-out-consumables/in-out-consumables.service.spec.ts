import { TestBed } from '@angular/core/testing';

import { InOutConsumablesService } from './in-out-consumables.service';

describe('InOutConsumablesService', () => {
  let service: InOutConsumablesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InOutConsumablesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
