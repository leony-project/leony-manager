import { Entrance } from './../../models/entrance';
import { InOutConsumablesService } from './../in-out-consumables.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/security/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AlertService } from 'src/app/components/alert/alerts.service';
import { Subject } from 'rxjs';
import { Consumable } from 'src/app/models/consumable';
import { ConsumablesService } from 'src/app/consumables/consumables.service';
import { TranslatorService } from 'src/app/translator/translator.service';



@Component({
  selector: 'app-in-out-consumables',
  templateUrl: './in-out-consumables.component.html',
  styleUrls: ['./in-out-consumables.component.scss']
})
export class InOutConsumablesComponent implements OnInit {
  public inOutForm: FormGroup;
  public newEntrance: Entrance;
  public selectedState: any = "State";
  public selectedConsumable = "Consommables";
  public errors = [];
  public filtredInOutConsumableList: Entrance[];
  public inOutConsumableList: Entrance[];
  public elementsPage = 1;
  public elementsPages: Array<number> = new Array<0>();
  public consumables: Consumable[] = [];
  public itemsNumberPerPage = 10;
  public consumablesTypeahead = new Subject<string>();



  constructor(
    public translator: TranslatorService,
    private inOutConsumablesServices: InOutConsumablesService,
    public auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private consumablesService: ConsumablesService,
    private alertService: AlertService) { }

  ngOnInit(): void {
    if (!this.auth.isAdmin())
      this.router.navigate(['/']);
    this.init();
    const formControls = {
      entrance: new FormControl(true, Validators.required),
      date: new FormControl(new Date(), Validators.required),
      consumable: new FormControl('Consommables', Validators.required),
      quantity: new FormControl(1, Validators.required)
    };
    this.inOutForm = this.formBuilder.group(formControls);
  }
  public init() {
    this.fetchConsumablesEntranceList();
    this.initConsumables();
  }
  public sortByDate(): void {
    this.filtredInOutConsumableList.sort((a, b) => {
      return b.date.getTime() - a.date.getTime();

    });
  }

  public filter() {
    if (this.selectedState == "State" && this.selectedConsumable == "Consommables")
      this.fetchConsumablesEntranceList();
    if (this.selectedState != "State" && this.selectedConsumable == "Consommables")
      this.filtredInOutConsumableList = this.inOutConsumableList.filter(s => s.entrance == this.selectedState);
    if (this.selectedState == "State" && this.selectedConsumable != "Consommables")
      this.filtredInOutConsumableList = this.inOutConsumableList.filter(c => c.consumable.name == this.selectedConsumable);
    if (this.selectedState != "State" && this.selectedConsumable != "Consommables")
      this.filtredInOutConsumableList = this.inOutConsumableList.filter(s => s.entrance == this.selectedState && s.consumable.name == this.selectedConsumable)
  }

  fetchConsumablesEntranceList() {
    this.inOutConsumablesServices.getAllInOutConsumables(this.elementsPage).subscribe((data: Entrance[]) => {
      this.elementsPages = Array.from(Array(Math.ceil(data['totalItems'] / this.itemsNumberPerPage)).keys());
      this.inOutConsumableList = data;
      this.filtredInOutConsumableList = this.inOutConsumableList;
    })
  }

  private initConsumables(): void {
    this.filterConsumables('');
    this.consumablesTypeahead.subscribe((term) => {
      this.filterConsumables(term);
    });
  }

  private filterConsumables(term): void {
    this.consumablesService.getFiltredConsumable(term).subscribe((consumables) => {
      this.consumables = consumables;
    });
  }

  delete(entrance: Entrance){
    
    let msg = this.translator.getSentence('ask-delete-this') + this.translator.getSentence('i/o-lower') + ' ? ' + this.translator.getSentence('irreversible-act');
    if (confirm(msg)) {
      this.inOutConsumablesServices.toggleInOutConsumableEnabled(entrance.id).subscribe(() =>{
        this.alertService.success(this.translator.getSentence('entrance-deleted'));
        this.fetchConsumablesEntranceList();
      },
      (error) =>{
        this.alertService.error(error);
      })
    }
  }

  updatePage(n): void {
    this.elementsPage = n;
    this.fetchConsumablesEntranceList();
  }

}
