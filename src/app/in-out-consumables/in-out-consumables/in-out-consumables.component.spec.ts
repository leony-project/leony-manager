import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InOutConsumablesComponent } from './in-out-consumables.component';

describe('InOutConsumablesComponent', () => {
  let component: InOutConsumablesComponent;
  let fixture: ComponentFixture<InOutConsumablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InOutConsumablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InOutConsumablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
