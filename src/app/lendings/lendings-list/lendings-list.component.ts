import { Component, OnInit } from '@angular/core';
import { Lending } from 'src/app/models/lending';
import { LendingsService } from 'src/app/lendings/lendings.service';
import { AuthService } from "../../security/auth.service";
import { faCube, faScrewdriver } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-lendings-list',
  templateUrl: './lendings-list.component.html',
  styleUrls: ['./lendings-list.component.scss']
})
export class LendingsListComponent implements OnInit {

  private itemsNumberPerPage = 10;


  public lendingsRequest: Lending[];
  public progressLendings: Lending[];
  public returnedLendings: Lending[];
  public lendings: Lending[];
  public myLendings: Lending[];
  public userLendings: Lending[];

  public lendingsRequestPage: number = 1;
  public progressLendingsPage: number = 1;
  public returnedLendingsPage: number = 1;
  public lendingsPage: number = 1;
  public myLendingsPage: number = 1;

  public lendingsRequestPages: Array<number> = new Array<0>();
  public progressLendingsPages: Array<number> = new Array<0>();
  public returnedLendingsPages: Array<number> = new Array<0>();
  public lendingsPages: Array<number> = new Array<0>();
  public myLendingsPages: Array<number> = new Array<0>();

  faCube = faCube;
  faScrewdriver = faScrewdriver;

  constructor(
    public translator: TranslatorService,
    public lendingsService: LendingsService,
    public auth: AuthService,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if (this.auth.isAdmin()) {
      this.loadAllLendingsRequest();
      this.loadAllLendingsNotReturned();
      this.loadAllLendingsReturned();
      this.loadAllLendings();
    } else {
      this.loadMyLendings();
    }
  }

  loadAllLendingsRequest() {
    this.lendingsService.getAllLendingsRequest(this.lendingsRequestPage).subscribe((lendings: Lending[]) => {
      this.lendingsRequestPages = Array.from(Array(Math.ceil(lendings['totalItems'] / this.itemsNumberPerPage)).keys());
      this.lendingsRequest = lendings;
      console.log(this.lendingsRequest)
    });
  }

  loadAllLendingsNotReturned() {
    this.lendingsService.getAllNotReturned(this.progressLendingsPage).subscribe((lendings: Lending[]) => {
      this.progressLendingsPages = Array.from(Array(Math.ceil(lendings['totalItems'] / this.itemsNumberPerPage)).keys());
      this.progressLendings = lendings;
    });
  }

  loadAllLendings() {
    this.lendingsService.getAll(this.lendingsPage).subscribe((lendings: Lending[]) => {
      this.lendingsPages = Array.from(Array(Math.ceil(lendings['totalItems'] / this.itemsNumberPerPage)).keys());
      this.lendings = lendings;
    });
  }


  loadMyLendings() {
    this.lendingsService.getAllMyLendings(this.myLendingsPage).subscribe((lendings: Lending[]) => {
      this.myLendingsPages = Array.from(Array(Math.ceil(lendings['totalItems'] / this.itemsNumberPerPage)).keys());
      this.lendings = lendings;
    });
  }

  loadUserLendings() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.lendingsService.getUserLendings(params.get('id')).subscribe((lendings: Lending[]) => {
        this.userLendings = lendings;
      });
    });
  }

  loadAllLendingsReturned() {
    this.lendingsService.getAllReturned(this.returnedLendingsPage).subscribe((lendings: Lending[]) => {
      this.returnedLendingsPages = Array.from(Array(Math.ceil(lendings['totalItems'] / this.itemsNumberPerPage)).keys());
      this.returnedLendings = lendings;
    });
  }

  updatePage(type, n) {
    switch (type) {
      case 'requestLendings':
        this.lendingsRequestPage = n;
        this.loadAllLendingsRequest();
        break;
      case 'progressLendings':
        this.progressLendingsPage = n;
        this.loadAllLendingsNotReturned();
        break;
      case 'myLendings':
        this.myLendingsPage = n;
        this.loadMyLendings();
        break;
      case 'returnedLendings':
        this.returnedLendingsPage = n;
        this.loadAllLendingsReturned();
        break;
    }
  }
}
