import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLendingsComponent } from './user-lendings.component';

describe('UserLendingsComponent', () => {
  let component: UserLendingsComponent;
  let fixture: ComponentFixture<UserLendingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserLendingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLendingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
