import { Component, OnInit } from '@angular/core';
import { TranslatorService } from 'src/app/translator/translator.service';
import { LendingsListComponent } from '../lendings-list.component';

@Component({
  selector: 'app-user-lendings',
  templateUrl: './user-lendings.component.html',
  styleUrls: ['./user-lendings.component.scss']
})
export class UserLendingsComponent extends LendingsListComponent implements OnInit {

  ngOnInit(): void {
    if (this.auth.isAdmin()) {
      this.loadUserLendings()
    }
  }
}



