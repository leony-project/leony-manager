import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Lending } from "../models/lending";
import { environment } from "../../environments/environment";
import { AuthService } from "../security/auth.service";
import * as moment from "moment";
import { Observable } from 'rxjs';
import { TranslatorService } from '../translator/translator.service';

@Injectable({
  providedIn: 'root'
})
export class LendingsService {

  apiURL = environment.rest_endpoint;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    public translator: TranslatorService
  ) { }

  getAll(page = 1): Observable<Lending[]> {
    return this.http.get<Lending[]>(this.apiURL + 'lendings?page=' + page);
  }

  getOneById(id): Observable<Lending> {
    return this.http.get<Lending>(this.apiURL + 'lendings/' + id);
  }

  getAllLendingsRequest(page = 1): Observable<Lending[]> {
    return this.http.get<Lending[]>(this.apiURL + 'lendings?accepted=false&page=' + page);
  }

  getAllNotReturned(page = 1): Observable<Lending[]> {
    return this.http.get<Lending[]>(this.apiURL + 'lendings?returned=false&accepted=true&page=' + page);
  }

  getAllMyLendings(page = 1): Observable<Lending[]> {
    return this.http.get<Lending[]>(this.apiURL + 'users/me/lendings?page=' + page);
  }

  getAllReturned(page = 1): Observable<Lending[]> {
    return this.http.get<Lending[]>(this.apiURL + 'lendings?returned=true&page=' + page);
  }

  acceptLendingRequest(lendingId: string) {
    return this.http.patch(this.apiURL + 'lendings/'+lendingId, {
      accepted: true,
      lendingDatetime: moment().format("YYYY-MM-DD HH:mm:ss")
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/merge-patch+json'
      })
    });
  }

  returnLending(lending: Lending) {
    return this.http.patch(this.apiURL + 'lendings/'+lending.id, {
      userReturn: lending.userReturn,
      lendingDatetime: lending.lendingDatetime,
      returnQuantity: lending.returnQuantity,
      returnDatetime: lending.returnDatetime,
      commentReturn: lending.commentReturn,
      returned: true
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/merge-patch+json'
      })
    });
  }

  delete(lendingId: string) {
    return this.http.delete(this.apiURL + 'lendings/'+lendingId);
  }

  toggleLendingsEnabled(lendingId): Observable<Lending> {
    return this.http.post<Lending>(`${this.apiURL}lendings/${lendingId}/toggle_enabled`, {});
  }

  getLendingDate(datestring): string {

    if(this.translator.local === "English")
    {
      return moment(datestring).format('MMM DD YYYY HH:mm');
    }
    else
    {
      console.log("here")
      return moment(datestring).format('DD MMM YYYY HH:mm');
    }
    
  }

  insert(value: any) {
    return this.http.post(this.apiURL + 'lendings', {
      userLending: value.user,
      entity: value.entity,
      consumable: value.consumable,
      lendingQuantity: value.lendingQuantity,
      lendingDatetime: value.lendingDatetime ? moment(value.lendingDatetime).format("YYYY-MM-DD HH:mm:ss") : moment().format("YYYY-MM-DD HH:mm:ss"),
      returnDatetime: value.returnDatetime ? moment(value.returnDatetime).format("YYYY-MM-DD HH:mm:ss") : null,
      commentLending: value.commentLending,
      accepted: this.auth.isAdmin(),
      returned: false,
      state: "toto",
      enabled: true
    });
  }


  getUserLendings(userid) {
    return this.http.get<Lending[]>(this.apiURL + `users/${userid}/lendings`)
  }

  getEntitiesLendings(id, page = 1): Observable<any> {
    return this.http.get<Lending[]>(this.apiURL + 'entities/'+id+'/lendings?page='+ page);
  }
}
