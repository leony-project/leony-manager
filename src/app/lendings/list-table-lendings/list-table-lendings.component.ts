import { Component, OnInit, Input } from '@angular/core';
import { LendingsService } from "../lendings.service";
import { AuthService } from "../../security/auth.service";
import { Lending } from "../../models/lending";
import { faCube, faScrewdriver } from '@fortawesome/free-solid-svg-icons';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'table[app-list-table-lendings]',
  templateUrl: './list-table-lendings.component.html',
  styleUrls: ['./list-table-lendings.component.scss']
})
export class ListTableLendingsComponent implements OnInit {

  @Input() lendings: Lending[];
  faCube = faCube;
  faScrewdriver = faScrewdriver;

  constructor(
    public translator: TranslatorService,
    public lendingsService: LendingsService,
    public auth: AuthService
  ) { }

  ngOnInit(): void {
  }

  deleteLending(lending: Lending) {
    this.lendingsService.toggleLendingsEnabled(lending.id).subscribe(() => {
      window.location.reload();
    });
  }

}
