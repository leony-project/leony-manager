import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTableLendingsComponent } from './list-table-lendings.component';

describe('ListTableLendingsComponent', () => {
  let component: ListTableLendingsComponent;
  let fixture: ComponentFixture<ListTableLendingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListTableLendingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTableLendingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
