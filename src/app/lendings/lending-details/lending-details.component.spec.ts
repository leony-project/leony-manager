import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LendingDetailsComponent } from './lending-details.component';

describe('LendingDetailsComponent', () => {
  let component: LendingDetailsComponent;
  let fixture: ComponentFixture<LendingDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LendingDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LendingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
