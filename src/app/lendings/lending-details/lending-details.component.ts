import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { LendingsService } from "../lendings.service";
import { Lending } from "../../models/lending";
import { Observable } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-lending-details',
  templateUrl: './lending-details.component.html',
  styleUrls: ['./lending-details.component.scss']
})
export class LendingDetailsComponent implements OnInit {

  public lending: Observable<Lending>;

  constructor(
    public translator: TranslatorService,
    public service: LendingsService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.loadLending()

  }

  private loadLending() {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.lending = this.service.getOneById(id)
  }

  acceptLendingRequest(lendingId: string) {
    this.service.acceptLendingRequest(lendingId).subscribe(() => {
    });
  }

  deleteLending(lendingId: string) {
    this.service.toggleLendingsEnabled(lendingId).subscribe(() => {
    });
  }

}
