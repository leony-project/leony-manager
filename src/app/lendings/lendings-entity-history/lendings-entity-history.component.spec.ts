import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LendingsEntityHistoryComponent } from './lendings-entity-history.component';

describe('LendingsEntityHistoryComponent', () => {
  let component: LendingsEntityHistoryComponent;
  let fixture: ComponentFixture<LendingsEntityHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LendingsEntityHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LendingsEntityHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
