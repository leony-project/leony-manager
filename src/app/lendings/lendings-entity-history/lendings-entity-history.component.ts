import { Component, OnInit } from '@angular/core';
import { LendingsService } from "../lendings.service";
import { ActivatedRoute } from "@angular/router";
import { Lending } from "../../models/lending";
import { EquipmentsService } from "../../equipments/equipments.service";
import { Entity } from "../../models/entity";
import { Observable } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-lendings-entity-history',
  templateUrl: './lendings-entity-history.component.html',
  styleUrls: ['./lendings-entity-history.component.scss']
})
export class LendingsEntityHistoryComponent implements OnInit {

  public lendings: Observable<Lending[]>;
  public entity: Observable<Entity>;
  entityId: string;
  public elementsPage = 1;
  public elementsPages: Array<number> = new Array<0>();
  private itemsNumberPerPage = 10

  constructor(
    public translator: TranslatorService,
    public service: LendingsService,
    private equipmentService: EquipmentsService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.entityId = params.get('id')
        this.loadEntity();
        this.loadLendings();
      }
    });
  }

  loadEntity() {
    this.entity = this.equipmentService.getOneEntity(this.entityId)
  }

  loadLendings(): void {
    this.service.getEntitiesLendings(this.entityId, this.elementsPage).subscribe((data) => {
      this.elementsPages = Array.from(Array(Math.ceil(data.totalItems / this.itemsNumberPerPage)).keys());
      this.lendings = data;
    });
  }

  updatePage(n): void {
    this.elementsPage = n;
    this.loadLendings();
  }

}
