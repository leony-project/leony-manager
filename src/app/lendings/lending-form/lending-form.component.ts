import { Component, OnInit } from '@angular/core';
import { LendingsService } from '../lendings.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { EquipmentsService } from '../../equipments/equipments.service';
import { Entity } from '../../models/entity';
import { AuthService } from '../../security/auth.service';
import { Select2OptionData } from 'ng-select2';
import { UsersService } from '../../users/users.service';
import { User } from '../../models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { ConsumablesService } from '../../consumables/consumables.service';
import { Consumable } from '../../models/consumable';
import { AlertService } from '../../components/alert/alerts.service';
import { Subject } from 'rxjs';
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-lending-form',
  templateUrl: './lending-form.component.html',
  styleUrls: ['./lending-form.component.scss']
})
export class LendingFormComponent implements OnInit {

  public lendingForm;
  public usersSelect: Array<Select2OptionData>;
  public entitySelect: Array<Select2OptionData>;
  public type = 'equipment';
  private formControls;
  public userTypeahead = new Subject<string>();
  public consumablesTypeahead = new Subject<string>();
  public entitiesTypeahead = new Subject<string>();
  public users: User[];
  public consumables: Consumable[] = [];
  public entities: Entity[] = [];
  public isChecked: boolean;
  public isDisabled: boolean;
  

  constructor(
    public translator: TranslatorService,
    private lendingsService: LendingsService,
    private equipmentsService: EquipmentsService,
    private consumableService: ConsumablesService,
    private usersService: UsersService,
    private formBuilder: FormBuilder,
    public auth: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.isDisabled = false;
    if (this.auth.isAdmin()) {
      this.initUsers();
    }
    this.initConsumables();
    this.initEntities();

    this.formControls = {
      user: new FormControl(this.auth.currentUserValue.id, Validators.required),
      entity: new FormControl(null),
      consumable: new FormControl(null),
      lendingQuantity: new FormControl(1, Validators.min(1)),
      lendingDatetime: new FormControl(null),
      returnDatetime: new FormControl(null),
      commentLending: new FormControl(null)
    };

    this.activatedRoute.paramMap.subscribe(params => {
      const type = this.router.url.split('/')[3];
      const id = params.get('id');
      if (id && type === 'equipment') { 
        this.isDisabled = true; 
        this.isChecked = false; 
        this.loadEquipmentParam(id); 
      }
      if (id && type === 'consumable') { 
        this.isDisabled = true; 
        this.isChecked = true; 
        this.loadConsumableParam(id); 
      }
    });

    if (this.auth.isAdmin()) { this.formControls.user = new FormControl('', Validators.required); }
    this.lendingForm = this.formBuilder.group(this.formControls);
  }


  private initConsumables(): void {
    this.filterConsumables('');
    this.consumablesTypeahead.subscribe((term) => {
      this.filterConsumables(term);
    });
  }

  private filterConsumables(term): void {
    this.consumableService.getFiltredConsumable(term).subscribe((consumables) => {
      this.consumables = consumables;
    });
  }

  private initUsers(): void {
    this.filterUsers('');
    this.userTypeahead.subscribe((term) => {
      this.filterUsers(term);
    });
  }

  private filterUsers(term): void {
    this.usersService.search(term).subscribe((users) => {
      this.users = users;
    });
  }

  private initEntities(): void {
    this.equipmentsService.getNotLendedsEntities().subscribe((entities) => {
      this.entities = entities;
    });
  }


  submit(): void {
    if (this.lendingForm.dirty && this.lendingForm.valid) {
      if (this.lendingForm.value.entity) { this.lendingForm.value.consumable = null; }
      if (this.lendingForm.value.consumable) { this.lendingForm.value.entity = null; }

      if (this.lendingForm.value.entity === null && this.lendingForm.value.consumable === null) {
        this.alertService.error(this.translator.getSentence('choose-entity-equipment'));
        return;
      }

      if (this.lendingForm.value.returnDatetime < this.lendingForm.value.lendingDatetime) {
        this.alertService.error(this.translator.getSentence('choose-valid-date'));
        return;
      }


      this.lendingsService.insert(this.lendingForm.value).subscribe(() => {
        this.router.navigate(['lendings']).then(() => { window.location.reload(); });
      }, error => {
        this.alertService.error(error.error['hydra:description']);
      });
    } else {
      this.translator.getSentence('entries-invalid');
    }
  }

  private loadEquipmentParam(id): void {
    this.equipmentsService.getAllEquipmentsEntities(id).subscribe((entities: Entity[]) => {
      entities = entities.filter(entity => {
        return entity.available;
      });
      if (entities.length > 0) {
        this.formControls.entity.setValue(entities[0].id);
      } else {
        this.alertService.error(this.translator.getSentence('no-entity-available'));
      }
    });
  }

  private loadConsumableParam(id): void {
    this.consumableService.getOne(id).subscribe((consumable: Consumable) => {
      this.type = 'consumable';
      this.consumables.push(consumable);
      this.formControls.consumable.setValue(consumable.id);
    });
  }

  public ChangedChecked(value: boolean) {
    if (value == true) {
      this.type = 'equipment';
    } else {
      this.type = 'consumable';
    }
  }

}
