import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LendingsService } from 'src/app/lendings/lendings.service';
import { Lending } from 'src/app/models/lending';
import { TranslatorService } from 'src/app/translator/translator.service';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../security/auth.service';

@Component({
  selector: 'app-lending-card',
  templateUrl: './lending-card.component.html',
  styleUrls: ['./lending-card.component.scss', '../../../style/card.scss']
})
export class LendingCardComponent implements OnInit {
  restBaseUri = environment.base_endpoint;

  @Input() lending: Lending;
  constructor(
    public translator: TranslatorService,
    public lendingsService: LendingsService,
    private router: Router,
    public auth: AuthService,
    public service: LendingsService,
  ) {
  }

  ngOnInit(): void {
  }


  updateLendingRequest(lending: Lending) {
    let msg = "";
    if (!lending.accepted) {
      msg = this.translator.getSentence('ask-accept-this') + this.translator.getSentence('lending-lower') + ' ? ';
      if (confirm(msg)) {
        this.lendingsService.acceptLendingRequest(lending.id).subscribe(() => {
          window.location.reload();
        });
      }
    } else if (lending.accepted) {
      msg = this.translator.getSentence('ask-retun-this') + this.translator.getSentence('lending-lower') + ' ? ';
      if (confirm(msg)) {
        this.lendingsService.returnLending(lending).subscribe(() => {
          this.router.navigate(['lendings/' + this.lending.id]);
        });
      }
    }
  }

  deleteLending(lendingId: string) {
    let msg = this.translator.getSentence('ask-refuse-this') + this.translator.getSentence('lending-lower') + ' ? ';
      
    if (confirm(msg)) {
      this.lendingsService.toggleLendingsEnabled(lendingId).subscribe(() => {
        window.location.reload();
      });
    }
  }
}
