import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LendingReturnFormComponent } from './lending-return-form.component';

describe('LendingReturnFormComponent', () => {
  let component: LendingReturnFormComponent;
  let fixture: ComponentFixture<LendingReturnFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LendingReturnFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LendingReturnFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
