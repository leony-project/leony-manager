import { Component, OnInit } from '@angular/core';
import { Lending } from "../../models/lending";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { LendingsService } from "../lendings.service";
import { AuthService } from "../../security/auth.service";
import { UsersService } from "../../users/users.service";
import { User } from "../../models/user";
import { Select2OptionData } from "ng-select2";
import { ActivatedRoute, Router } from "@angular/router";
import * as moment from "moment";
import { ConsumablesService } from "../../consumables/consumables.service";
import { AlertService } from "../../components/alert/alerts.service";
import { TranslatorService } from 'src/app/translator/translator.service';

@Component({
  selector: 'app-lending-return-form',
  templateUrl: './lending-return-form.component.html',
  styleUrls: ['./lending-return-form.component.scss']
})
export class LendingReturnFormComponent implements OnInit {

  public lending: Lending;
  public returnLendingForm: FormGroup;
  public usersSelect: Array<Select2OptionData>;
  public validationForm: boolean;
  public userDefaultValue: 'Bodnar Hugo';

  constructor(
    public translator: TranslatorService,
    private activatedRoute: ActivatedRoute,
    public lendingsService: LendingsService,
    public auth: AuthService,
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private consumableService: ConsumablesService,
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.validationForm = true;
    this.activatedRoute.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.loadLending(params.get('id'));
        this.loadUsers();
        const formControls = {
          userReturn: new FormControl('', Validators.required),
          lendingDatetime: new FormControl(''),
          lendingQuantity: new FormControl(1, Validators.min(1)),
          returnQuantity: new FormControl(0, Validators.min(0)),
          returnDatetime: new FormControl(moment().format()),
          stateReturn: new FormControl(null),
          commentReturn: new FormControl('')
        };
        this.returnLendingForm = this.formBuilder.group(formControls);
      } else {
        this.router.navigate(['lendings']).then(() => { window.location.reload(); });
      }
    });
  }

  private loadUsers() {
    this.usersService.getAll().subscribe((data: User[]) => {
      this.usersSelect = [];
      for (let user of data) {
        this.usersSelect.push({
          id: user.id,
          text: user.firstName + ' ' + user.lastName
        })
      }
    });
  }

  private loadLending(id: string) {
    this.lendingsService.getOneById(id).subscribe((lending: Lending) => {
      this.lending = lending;
      this.returnLendingForm.controls.lendingDatetime.patchValue(this.lending.lendingDatetime);
      if (this.lending.consumable) this.returnLendingForm.controls.lendingQuantity.patchValue(this.lending.lendingQuantity);
    });
  }

  public confirmReturn() {
    this.validationForm = true;
    if (this.returnLendingForm.dirty && this.returnLendingForm.valid) {
      this.lending.userReturn = this.returnLendingForm.value.userReturn;
      this.lending.lendingDatetime = this.returnLendingForm.value.lendingDatetime;
      this.lending.returnDatetime = this.returnLendingForm.value.returnDatetime;
      this.lending.commentReturn = this.returnLendingForm.value.commentReturn;
      if (this.lending.consumable) this.lending.returnQuantity = this.returnLendingForm.value.returnQuantity;
      if (this.lending.consumable) this.lending.lendingQuantity = this.returnLendingForm.value.lendingQuantity;
      this.lending.returned = true;

      this.lendingsService.returnLending(this.lending).subscribe(() => {
        this.router.navigate(['lendings/' + this.lending.id]).then(() => { window.location.reload(); });
      });

    } else {
      this.alertService.error(this.translator.getSentence('multiple-entries'))
    }
  }

}
