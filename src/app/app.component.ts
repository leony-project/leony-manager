import { Component } from '@angular/core';
import { AuthService } from "./security/auth.service";
import { User } from "./models/user";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = "Leony Manager";

  constructor(
    private authService: AuthService,
    public router: Router
    ) { }

  ngOnInit(): void {
    if(this.authService.isConnected()) this.updateCurrentUser();
  }

  updateCurrentUser() {
    this.authService.getCurrentUser().subscribe((user: User) => {
      localStorage.setItem('currentUser', JSON.stringify(user));
    }, () => {
      this.authService.logout();
    });
  }

  checkConnected(): boolean{
    if (this.authService.isConnected())
      return true;
    return false;
  }

}
