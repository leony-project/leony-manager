import { Component, OnInit } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { TranslatorService } from '../translator.service';
import { DateTimeAdapter } from 'ng-pick-datetime';

@Component({
  selector: 'app-translator-button',
  templateUrl: './translator-button.component.html',
  styleUrls: ['./translator-button.component.scss']
})
export class TranslatorButtonComponent implements OnInit {
  faChevronDown = faChevronDown;

  constructor(public translator: TranslatorService, public dateTimeAdapter: DateTimeAdapter<any>) {
  }

  ngOnInit(): void {
  }

  public changeLanguage(language: string) {
    this.translator.local = language;
    this.dateTimeAdapter.setLocale(language);
    console.log(language);
  }

}
