import TradJson from '../../assets/trad.json'
import { Injectable } from '@angular/core';


interface Sentence {
    key: string;
    fr: string;
    en: string;
}

class Local {
    name: string;
    prefix: string;
}

class TranslatorData {
    sentences: Sentence[];
    availableLocals: Local[];
}



//Map all sentences contained in the filetrad.json. If you want to add a new language, just add the translations in trad.json and the new language at the top of the file (default language is the first of the list)

@Injectable({
    providedIn: 'root'
})
export class TranslatorService {
    private _translatorData: TranslatorData = TradJson
    private _local = this._translatorData.availableLocals[0];

    constructor() {
        console.log(this._translatorData)
    }

    getSentence(id: string): string {
        var result = this._translatorData.sentences.find(element => element.key === id);

        if (result == null) {
            return ""
        }

        type ObjectKey = keyof typeof result;
        const myVar = this._local.prefix as ObjectKey;
        return result[myVar]
    }

    get local(): string {
        return this._local.name
    }

    get availableLocalsExceptCurrent(): Local[] {
        return this._translatorData.availableLocals.filter(el => el != this._local)
    }

    set local(local: string) {
        var result = this._translatorData.availableLocals.find(element => element.prefix === local);

        if (result == null) {
            throw 'Local not available';
        }


        this._local = this._translatorData.availableLocals.find(element => element.prefix === local);
    }
}