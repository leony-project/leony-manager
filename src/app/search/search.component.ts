import { Entity } from 'src/app/models/entity';
import { AuthService } from './../security/auth.service';
import { ConsumablesService } from 'src/app/consumables/consumables.service';
import { EquipmentsService } from 'src/app/equipments/equipments.service';
import { Consumable } from 'src/app/models/consumable';
import { Component, OnInit } from '@angular/core';
import { Equipment } from '../models/equipment';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslatorService } from '../translator/translator.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  listeEquipement: Equipment[];
  listeConsumable: Consumable[];
  listeEntity: Entity[];
  searchCriteria: string;
  private sub: any;

  constructor(private router: Router,
    public translator: TranslatorService,
    private route: ActivatedRoute,
    private equipmentService: EquipmentsService,
    private consumableService: ConsumablesService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.searchCriteria = ""
    this.listeConsumable = [];
    this.listeEquipement = [];
    this.listeEntity = [];
    this.route.paramMap.subscribe(params => {
      this.searchCriteria = params.get('searchCriteria');
      this.searchEquipment();
      this.searchConsumable();
      this.searchEntity();
    });
  }

  searchEquipment() {
    this.equipmentService.getFiltredEquipment(this.searchCriteria).subscribe(data => {
      if (data) {
        this.listeEquipement = data;
      }
    });
  }
  searchEntity() {
    this.equipmentService.getFiltredEntity(this.searchCriteria).subscribe(data => {
      if (data)
        this.listeEntity = data;
    });
  }

  searchConsumable() {
    this.consumableService.getFiltredConsumable(this.searchCriteria).subscribe(data => {
      if (data)
        this.listeConsumable = data;
    });
  }


}
