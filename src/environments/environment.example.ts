export const environment = {
    name: "", // Env name (prod, staging, etc..)
    production: false, // Production or Development
    rest_endpoint: '', // URL API REST
    base_endpoint: '', // URL API BASE
  };